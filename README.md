#GYRO

Every header (includes h2 and h4) = 1 slide


##What is Gyro? (Marc-Andre)
Gyro is...


####Authentication process (Christian)
Le processus d'authentication contient de deux aspects: La creation d'un compte et la connection.
- **Register**:
    - Username must be between 4 and 32 characters
    - Password must be at least 8 characters
- Login
- Demo

####Home page (Marc-Andre)


####Creating a project (Nicolas)
- **Who can create a project**:
- **Who has access to your project**:
   
####Adding a member to your project (Nicolas)
- **What permissions do project members have**:
- Demo

####Creating your first story (Jess)
- **What is a story**:
- **What are assignees**:
- Demo

####Adding a subtask to your story (Jess)
- **What is a subtask?**: Ce sont les tâches à faire pour compléter une story. C'est comme une story divisée en plusieurs parties. Une tâche est associée à une story et a un titre et un status.
- **What's the difference between a subtask and a story?**: Une story est composée de plusieurs tâches. Une tâche n'est pas un container, contrairement à une story. Une tâche ne peut pas avoir un utilisateur assigné.
- Demo

####Classifying your stories with Epics (Christian)
- **What is an epic?**: An epic is like a folder of stories
- **When to use epics?**: When you have multiple stories that affect a similar aspect. That way, you can filter the stories in a project by the epic and easily identify different parts of the project.
    - ex: [4 stories: Create, Read, Update, Delete] -> Epic CRUD
- Demo

##Difficulties during the development process
- Different IDE (We fixed this by all switching to IntelliJ!)
- Different programming styles

##Future features (Christian)
- Sprints
- Reports
- Contribution from each project member


##Conclusion (Marc-Andre)

