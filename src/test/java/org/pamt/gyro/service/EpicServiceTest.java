package org.pamt.gyro.service;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pamt.gyro.bean.Epic;
import org.pamt.gyro.bean.Project;
import org.pamt.gyro.bean.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EpicServiceTest {

    private static final Long TEST_PROJECT_ID = 1L;

    @Autowired
    private EpicService epicService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private StoryService storyService;




    @Before
    public void setUp() {

    }


    @After
    public void tearDown() {
        epicService = null;
    }


    @Test
    public void createEpic() {
        User user = new User("TEST_USERNAME", "TEST_PASSWORD", "TEST@EMAIL.COM", false);
        Project project = new Project();
        //project.setProject(TEST_PROJECT_ID);
        project.setTitle("PROJECT_TITLE");
        project.setDescription("PROJECT_DESC");
        project.setOwnerUsername(user.getUsername());
        String result = epicService.createEpic(user, "STORY_TITLE", "STORY_DESCRIPTION", TEST_PROJECT_ID);
        JsonObject json = new Gson().fromJson(result, JsonObject.class);
        Long epicId = Long.parseLong(json.get("ID").toString());
        assertNotNull("Failed to create story", epicService.getEpicById(epicId));
        epicService.deleteEpic(epicId, user);
        assertNull("Failed to delete test story", epicService.getEpicById(epicId));
    }


    @Test
    public void createEpicWhenUserNotConnected() {
        Project project = new Project();
        project.setProjectId(TEST_PROJECT_ID);
        project.setTitle("PROJECT_TITLE");
        project.setDescription("PROJECT_DESC");
        String result = epicService.createEpic(null, "STORY_TITLE", "STORY_DESCRIPTION", TEST_PROJECT_ID);
        JsonObject json = new Gson().fromJson(result, JsonObject.class);
        boolean isError = Boolean.parseBoolean(json.get("ERROR").toString());
        assertTrue("Created a story even though user is null (not connected)", isError);
    }


    @Test
    public void getEpicsForProject() {
        User user = new User("TEST_USERNAME", "TEST_PASSWORD", "TEST@EMAIL.COM", false);
        String projectResult = projectService.createProject(user, "TEST_TITLE", "TEST_DESCRIPTION");
        Long projectId = Long.parseLong(new Gson().fromJson(projectResult, JsonObject.class).get("ID").toString());
        String story1Result = epicService.createEpic(user, "STORY_TITLE_1", "STORY_DESCRIPTION_1", projectId);
        String story2Result = epicService.createEpic(user, "STORY_TITLE_2", "STORY_DESCRIPTION_2", projectId);
        Long epic1Id = Long.parseLong(new Gson().fromJson(story1Result, JsonObject.class).get("ID").toString());
        Long epic2Id = Long.parseLong(new Gson().fromJson(story2Result, JsonObject.class).get("ID").toString());
        assertTrue("Failed to get stories by project Id (1)",
                epicService.getEpicsByProjectId(projectId).contains(epicService.getEpicById(epic1Id)));
        assertTrue("Failed to get stories by project Id (2)",
                epicService.getEpicsByProjectId(projectId).contains(epicService.getEpicById(epic2Id)));
    }


    @Test
    public void addStoryToEpic(){
        User user = new User("TEST_USERNAME", "TEST_PASSWORD", "TEST@EMAIL.COM", false);
        Project project = new Project();
        //project.setProjectId(TEST_PROJECT_ID);
        project.setTitle("PROJECT_TITLE");
        project.setDescription("PROJECT_DESC");
        project.setOwnerUsername(user.getUsername());
        String resultProject = projectService.createProject(user, project.getTitle(), project.getDescription());
        Long projectId = Long.parseLong(new Gson().fromJson(resultProject, JsonObject.class).get("ID").toString());
        String resultEpic = epicService.createEpic(user, "EPIC_TITLE", "EPIC_DESCRIPTION", projectId);
        String resultStory = storyService.createStory(user, "STORY_TITLE", "STORY_DESCRIPTION", projectId);
        Long epic1Id = Long.parseLong(new Gson().fromJson(resultEpic, JsonObject.class).get("ID").toString());
        Long storyId = Long.parseLong(new Gson().fromJson(resultStory, JsonObject.class).get("ID").toString());
        String result = epicService.addStoryToEpic(epic1Id, storyId);
        JsonObject json = new Gson().fromJson(result, JsonObject.class);
        boolean isError = Boolean.parseBoolean(json.get("ERROR").toString());
        assertFalse("Failed to add story to epic", isError);
    }

    @Test
    public void deleteEpic() {
        User user = new User("TEST_USERNAME", "TEST_PASSWORD", "TEST@EMAIL.COM", false);
        projectService.createProject(user, "PROJECT_TITLE", "PROJECT_DESC");
        Project project = projectService.getProjectsByTitle("PROJECT_TITLE").get(0);

        epicService.createEpic(user, "EPIC_TITLE", "EPIC_DESC", project.getProjectId());
        Epic epic = epicService.getEpicsByProjectId(project.getProjectId()).get(0);

        String result = epicService.deleteEpic(100000L, user);
        assertTrue("The epic doesn't exist", Boolean.parseBoolean(this.readPropertyFromJson(result, "ERROR")));

        result = epicService.deleteEpic(epic.getEpicId(), null);
        assertTrue("The user doesn't exist", Boolean.parseBoolean(this.readPropertyFromJson(result, "ERROR")));

        epicService.deleteEpic(epic.getEpicId(), user);
        assertNull("The epic has been deleted", epicService.getEpicById(epic.getEpicId()));
    }

    private String readPropertyFromJson(String str, String property) {
        return new Gson().fromJson(str, JsonObject.class).get(property).toString();
    }
}
