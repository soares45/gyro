package org.pamt.gyro.service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pamt.gyro.bean.Project;
import org.pamt.gyro.bean.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;


/*

FIXME: since moved from singleton to @Autowire JPA, tests are BROKEN

 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class ProjectServiceTest {
	
	@Autowired
	private ProjectService projectService;
	
	@Before
	public void setUp() {

	}
	
	
	@After
	public void tearDown() {
		projectService = null;
	}
	
	
	@Test
	public void createProject() {
		User user = getUser("TEST_USER");
		String result = projectService.createProject(user, "TEST_TITLE", "TEST_DESCRIPTION");
		String resultMessage = ((new Gson().fromJson(result, JsonObject.class)).get("MESSAGE")).toString();
		assertEquals("Failed to create project", resultMessage, "\"Project created successfully\"");
	}
	
	
	@Test
	public void createProjectWhenUserNotConnected() {
		String result = projectService.createProject(null, "TEST_TITLE", "TEST_DESCRIPTION");
		boolean isError = Boolean.parseBoolean(new Gson().fromJson(result, JsonObject.class).get("ERROR").toString());
		assertTrue("Created project even though user is null (not connected)", isError);
	}

	@Test
	public void getOwnedProjectsByUserTest() {
		User user1 = getUser("TEST_USER_1");
		addProject(user1, "TEST_PROJECT_1");

		User user2 = getUser("TEST_USER_2");
		addProject(user2, "TEST_PROJECT_2");

		String result = projectService.getProjectsByUser(user1);
		JsonArray projects = new Gson().fromJson(result, JsonObject.class).getAsJsonArray("PROJECTS");
		assertEquals(1, projects.size());
	}

	@Test
	public void getOwnedProjectsByUserTestUserNull() {
		String result = projectService.getProjectsByUser(null);
		boolean isError = Boolean.parseBoolean(new Gson().fromJson(result, JsonObject.class).get("ERROR").toString());
		assertTrue("Get projects of a user that does not exists", isError);
	}

	@Test
	public void getOwnedProjectsByUserTestUserNoProjects() {
		User user = getUser("TEST_USER");
		String result = projectService.getProjectsByUser(user);
		JsonArray projects = new Gson().fromJson(result, JsonObject.class).getAsJsonArray("PROJECTS");
		assertEquals(0, projects.size());
	}
	
	private User getUser(String username) {
		return new User(username, username, username + "@hotmail.com", false);
	}

	
	private void addProject(User user, String title) {
		projectService.createProject(user, title, title);
	}
}


