package org.pamt.gyro.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pamt.gyro.bean.Status;
import org.pamt.gyro.bean.Story;
import org.pamt.gyro.bean.SubTask;
import org.pamt.gyro.bean.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SubTaskServiceTest {

    @Autowired
    private StoryService storyService;

    @Autowired
    private SubTaskService subTaskService;

    @Autowired
    private UserService userService;

    @Test
    public void createSubTask() {
        SubTask subTask = new SubTask();
        subTask.setTitle("SUBTASK_TITLE");
        subTask.setStatus(Status.TODO);
        User user = new User("USER_USERNAME", "USER_PASSWORD", "USER_EMAIL", false);
        storyService.createStory(user, "STORY_TITLE", "STORY_DESCRIPTION", 1L);
        Story story = storyService.getStoriesByProjectId(1L).get(0);
        String result = subTaskService.createSubTask(story.getStoryId(), "SubTask_TITLE");
        JsonObject json = new Gson().fromJson(result, JsonObject.class);
        boolean isError = Boolean.parseBoolean(json.get("ERROR").toString());
        Long subTaskId = Long.parseLong(json.get("ID").toString());
        assertFalse("Can't create SubTask", isError);

    }

    @Test
    public void getSubTaskById() {
        SubTask subTask = new SubTask();
        subTask.setTitle("SUBTASK_TITLE");
        subTask.setStatus(Status.TODO);
        User user = new User("USER_USERNAME", "USER_PASSWORD", "USER_EMAIL", false);
        storyService.createStory(user, "STORY_TITLE", "STORY_DESCRIPTION", new Long(1));
        Story story = storyService.getStoriesByProjectId(new Long(1)).get(0);
        String result = subTaskService.createSubTask(story.getStoryId(), "SubTask_TITLE");
        JsonObject json = new Gson().fromJson(result, JsonObject.class);
        Long subTaskId = Long.parseLong(json.get("ID").toString());
        SubTask subTaskFound = subTaskService.getSubTaskById(subTaskId);
        assertNotNull("Can't get subTask with ID", subTaskFound);
    }

    @Test
    public void getSubTasksByStoryId() {
        SubTask subTask = new SubTask();
        subTask.setTitle("SUBTASK_TITLE");
        subTask.setStatus(Status.TODO);
        User user = new User("USER_USERNAME", "USER_PASSWORD", "USER_EMAIL", false);
        storyService.createStory(user, "STORY_TITLE", "STORY_DESCRIPTION", 1L);
        Story story = storyService.getStoriesByProjectId(1L).get(0);
        String result = subTaskService.createSubTask(story.getStoryId(), "SubTask_TITLE");
        List<SubTask> subTaskFound = subTaskService.getSubTasksByStoryId(story.getStoryId());
        assertNotNull("Can't get subTask with ID", subTaskFound);
    }

    @Test
    public void deleteSubTask() {
        SubTask subTask = new SubTask();
        subTask.setTitle("SUBTASK_TITLE");
        subTask.setStatus(Status.TODO);
        User user = new User("USER_USERNAME", "USER_PASSWORD", "USER_EMAIL", false);
        storyService.createStory(user, "STORY_TITLE", "STORY_DESCRIPTION", 1L);
        Story story = storyService.getStoriesByProjectId(1L).get(0);
        String result = subTaskService.createSubTask(story.getStoryId(), "SUBTASK_TITLE");
        JsonObject json = new Gson().fromJson(result, JsonObject.class);
        Long storyId = Long.parseLong(json.get("ID").toString());
        subTaskService.deleteSubTask(storyId);
        SubTask subTaskDeleted = subTaskService.getSubTaskById(storyId);
        assertNull("The subtask was not deleted", subTaskDeleted);
    }

    @Test
    public void updateSubTaskStatus() {
        User user = new User("USER_USERNAME", "USER_PASSWORD", "USER_EMAIL", false);
        storyService.createStory(user, "STORY_TITLE", "STORY_DESCRIPTION", 1L);
        Long subTaskId = Long.valueOf(this.readPropertyFromJson(subTaskService.createSubTask(1L, "SUBTASK_TITLE"), "ID"));

        subTaskService.updateSubTaskStatus(subTaskId, Status.IN_PROGRESS);
        SubTask subTask = subTaskService.getSubTaskById(subTaskId);
        assertEquals(subTask.getStatus(), Status.IN_PROGRESS);

        String result = subTaskService.updateSubTaskStatus(1000000L, Status.IN_PROGRESS);
        assertEquals("The subtask doesn't exist", Boolean.parseBoolean(this.readPropertyFromJson(result, "ERROR")), true);
    }

    @Test
    public void updateSubTask() {
        User user = new User("USER_USERNAME", "USER_PASSWORD", "USER_EMAIL", false);
        storyService.createStory(user, "STORY_TITLE", "STORY_DESCRIPTION", 1L);
        Long subTaskId = Long.valueOf(this.readPropertyFromJson(subTaskService.createSubTask(1L, "SUBTASK_TITLE"), "ID"));

        subTaskService.updateSubTask(subTaskId, "NEW_TITLE");
        SubTask subTask = subTaskService.getSubTaskById(subTaskId);
        assertEquals(subTask.getTitle(), "NEW_TITLE");

       String result = subTaskService.updateSubTask(10000L, "NEW_TITLE");
       assertTrue("The subtask doesn't exist", Boolean.parseBoolean(this.readPropertyFromJson(result, "ERROR")));

       result = subTaskService.updateSubTask(subTaskId, "ABC");
       assertTrue("Subtask title too short", Boolean.parseBoolean(this.readPropertyFromJson(result, "ERROR")));
    }

    private String readPropertyFromJson(String str, String property) {
        return new Gson().fromJson(str, JsonObject.class).get(property).toString();
    }
}


