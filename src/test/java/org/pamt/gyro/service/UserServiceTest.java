package org.pamt.gyro.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pamt.gyro.util.HashUtils;
import org.pamt.gyro.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;
import static org.pamt.gyro.util.Utils.checkIfJSONReplyHasErrorAsTrue;


@RunWith(SpringRunner.class)
@DataJpaTest
public class UserServiceTest {
	
	@Autowired private UserService userService;
	
	
	@Before
	public void setUp() {

	}
	
	
	@After
	public void tearDown() {
	
	}
	
	
	@Test
	public void register() {
		userService.addUser("TEST_USERNAME2", "TEST_PASSWORD", "TEST@EMAIL.COM");
		assertTrue("Failed to create user", userService.getUserByUsername("TEST_USERNAME2") != null);
		// By default, spring boot should rollback the transaction, but we'll just double check
		userService.deleteUser("TEST_USERNAME2");
		assertTrue("Failed to delete test user", userService.getUserByUsername("TEST_USERNAME2") == null);
	}
	
	
	@Test
	public void login() {
		String hashedPassword = HashUtils.sha256("TEST_PASSWORD");
		userService.addUser("TEST_USERNAME2", hashedPassword, "TEST@EMAIL.COM");
		String result = userService.login("TEST_USERNAME2", hashedPassword, null);
		System.out.println(result);
	}
	
	
	@Test
	public void registerWithUsernameTooShort() {
		String output = userService.addUser("1", "TEST_PASSWORD", "TEST@EMAIL.COM");
		assertTrue("Username must have a length of at least " + UserService.MIN_USERNAME_LENGTH,
			  checkIfJSONReplyHasErrorAsTrue(output));
	}
	
	
	@Test
	public void registerWithUsernameTooLong() {
		String output = userService.addUser("111111111111111111111111111111111", "TEST_PASSWORD", "TEST@EMAIL.COM");
		assertTrue("Username must have a length of at most " + UserService.MAX_USERNAME_LENGTH,
			  checkIfJSONReplyHasErrorAsTrue(output));
	}
	
	
}
