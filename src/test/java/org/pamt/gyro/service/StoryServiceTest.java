package org.pamt.gyro.service;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pamt.gyro.bean.Project;
import org.pamt.gyro.bean.Story;
import org.pamt.gyro.bean.User;
import org.pamt.gyro.repository.StoryRepository;
import org.pamt.gyro.repository.UserRepository;
import org.skyscreamer.jsonassert.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

/*

FIXME: since moved from singleton to @Autowire JPA, tests are BROKEN

 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class StoryServiceTest {

    private static final Long TEST_PROJECT_ID = 1L;

    @Autowired
    private StoryService storyService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserService userService;


    @Before
    public void setUp() {

    }


    @After
    public void tearDown() {

    }


    @Test
    public void createStory() {
        //User user = new User("TEST_USERNAME", "TEST_PASSWORD", "TEST@EMAIL.COM", false);
        //Project project = new Project();
        //project.setProject(TEST_PROJECT_ID);
        //project.setTitle("PROJECT_TITLE");
        //project.setDescription("PROJECT_DESC");
        //project.setOwnerUsername(user.getUsername());
        User user = userService.getUserByUsername("test");
        JsonObject jo = new Gson().fromJson(projectService.createProject(user, "PROJECT_TITLE", "PROJECT_DESC"), JsonObject.class);
        Long projectId = Long.parseLong(jo.get("ID").toString());
        System.out.println(projectId);
        String result = storyService.createStory(user, "STORY_TITLE", "STORY_DESCRIPTION", projectId);
        System.out.println(result);
        JsonObject json = new Gson().fromJson(result, JsonObject.class);
        Long storyId = Long.parseLong(json.get("ID").toString());
        assertNotNull("Failed to create story", storyService.getStoryById(storyId));
        storyService.deleteStory(storyId, user);
        assertNull("Failed to delete test story", storyService.getStoryById(storyId));
    }


    @Test
    public void createStoryWhenUserNotConnected() {
        Project project = new Project();
        project.setProjectId(TEST_PROJECT_ID);
        project.setTitle("PROJECT_TITLE");
        project.setDescription("PROJECT_DESC");
        String result = storyService.createStory(null, "STORY_TITLE", "STORY_DESCRIPTION", TEST_PROJECT_ID);
        JsonObject json = new Gson().fromJson(result, JsonObject.class);
        boolean isError = Boolean.parseBoolean(json.get("ERROR").toString());
        assertTrue("Created a story even though user is null (not connected)", isError);
    }


    @Test
    public void getStoriesForProject() {
        User user = new User("TEST_USERNAME", "TEST_PASSWORD", "TEST@EMAIL.COM", false);
        String projectResult = projectService.createProject(user, "TEST_TITLE", "TEST_DESCRIPTION");
        Long projectId = Long.parseLong(new Gson().fromJson(projectResult, JsonObject.class).get("ID").toString());
        String story1Result = storyService.createStory(user, "STORY_TITLE_1", "STORY_DESCRIPTION_1", projectId);
        String story2Result = storyService.createStory(user, "STORY_TITLE_2", "STORY_DESCRIPTION_2", projectId);
        Long story1Id = Long.parseLong(new Gson().fromJson(story1Result, JsonObject.class).get("ID").toString());
        Long story2Id = Long.parseLong(new Gson().fromJson(story2Result, JsonObject.class).get("ID").toString());
        assertTrue("Failed to get stories by project Id (1)",
                storyService.getStoriesByProjectId(projectId).contains(storyService.getStoryById(story1Id)));
        assertTrue("Failed to get stories by project Id (2)",
                storyService.getStoriesByProjectId(projectId).contains(storyService.getStoryById(story2Id)));
    }

    @Test
    public void assignUserToStoryTest() {
        userService.addUser("TEST_USERNAME", "TEST_PASSWORD", "TEST@EMAIL.COM");

        User user = userService.getUserByUsername("TEST_USERNAME");
        projectService.createProject(user, "PROJECT_TITLE", "PROJECT_DESCRIPTION");

        List<Project> projects = projectService.getProjectsByTitle("PROJECT_TITLE");
        String result = storyService.createStory(user, "STORY_TITLE", "STORY_DESCRIPTION", projects.get(0).getProjectId());

        Long storyId = Long.valueOf(this.readPropertyFromJson(result, "ID"));
        Story story = storyService.getStoryById(storyId);
        assertFalse("Successfully assigned user", Boolean.parseBoolean(this.readPropertyFromJson(storyService.assignUser(user.getUsername(), storyId), "ERROR")));
        assertEquals("User is assigned to story", story.getAssigneeUsername(), user.getUsername());
        assertTrue("Failed to assign user that doesn't exist", Boolean.parseBoolean(this.readPropertyFromJson(storyService.assignUser("WRONG_USERNAME", storyId), "ERROR")));
        assertTrue("Failed to assign user to story that doesn't exist", Boolean.parseBoolean(this.readPropertyFromJson(storyService.assignUser(user.getUsername(), 100000l), "ERROR")));
    }

    @Test
    public void unassignStoryTest() {
        userService.addUser("TEST_USERNAME", "TEST_PASSWORD", "TEST@EMAIL.COM");

        User user = userService.getUserByUsername("TEST_USERNAME");
        projectService.createProject(user, "PROJECT_TITLE", "PROJECT_DESCRIPTION");

        List<Project> projects = projectService.getProjectsByTitle("PROJECT_TITLE");
        String result = storyService.createStory(user, "STORY_TITLE", "STORY_DESCRIPTION", projects.get(0).getProjectId());

        Long storyId = Long.valueOf(this.readPropertyFromJson(result, "ID"));
        storyService.assignUser(user.getUsername(), storyId);
        Story story = storyService.getStoryById(storyId);

        assertFalse("No errors are returned", Boolean.parseBoolean(this.readPropertyFromJson(storyService.unassignStory(storyId), "ERROR")));
        assertNull("Nobody is assigned to the story", story.getAssigneeUsername());
        assertTrue("Failed to unassign non existant story", Boolean.parseBoolean(this.readPropertyFromJson(storyService.unassignStory(1000000l), "ERROR")));
    }

    private String readPropertyFromJson(String str, String property) {
        return new Gson().fromJson(str, JsonObject.class).get(property).toString();
    }
}