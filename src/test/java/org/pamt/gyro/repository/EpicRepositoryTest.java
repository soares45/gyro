package org.pamt.gyro.repository;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.pamt.gyro.bean.Epic;
import org.pamt.gyro.bean.Project;
import org.pamt.gyro.bean.Story;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EpicRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private EpicRepository epicRepository;

    @Autowired
    private ProjectRepository projectRepository;


    @Test
    public void findAllByProjectId() throws Exception {
        Project project = new Project();
        project.setTitle("Gyro");
        project.setOwnerUsername("johndoe");
        projectRepository.save(project);
        Epic epic = new Epic();
        epic.setTitle("TEST_EPIC_TITLE");
        entityManager.merge(project);
        epic.setProject(project);
        entityManager.merge(epic);
        List<Epic> epics = epicRepository.findAllByProject(project);
        for (int i = 0; i < epics.size(); i++) {
            assertEquals("Failed to find all stories by projectId", epics.get(i).getTitle(), epic.getTitle());
        }
    }


    @Test
    public void findByEpicId() throws Exception {
        Epic epic = new Epic();
        epic.setTitle("TEST_EPIC_TITLE");
        entityManager.persist(epic);
        Epic epicFound = epicRepository.findByEpicId(epic.getEpicId());
        assertEquals("Failed to find epic by epicId", epicFound, epic);
    }


}
