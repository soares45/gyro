package org.pamt.gyro.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.pamt.gyro.bean.Project;
import org.pamt.gyro.bean.Story;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class StoryRepositoryTest {
	
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private StoryRepository storyRepository;
	
	@Autowired
	private ProjectRepository projectRepository;
	
	
	@Test
	public void findAllByProject() throws Exception {
		Project project = new Project();
		project.setTitle("Gyro");
		project.setOwnerUsername("johndoe");
		projectRepository.save(project);
		Story story = new Story();
		story.setTitle("TEST_STORY_TITLE");
		entityManager.merge(project);
		story.setProject(project);
		entityManager.merge(story);
		List<Story> stories = storyRepository.findAllByProject(project);
		for (int i = 0; i < stories.size(); i++) {
			assertEquals("Failed to find all stories by projectId", stories.get(i).getTitle(), story.getTitle());
		}
	}
	
	
	@Test
	public void findByStoryId() throws Exception {
		Story story = new Story();
		story.setTitle("TEST_STORY_TITLE");
		entityManager.persist(story);
		Story storyFound = storyRepository.findByStoryId(story.getStoryId());
		assertEquals("Failed to find story by storyId", storyFound, story);
	}
	
}