package org.pamt.gyro.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pamt.gyro.bean.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private UserRepository userRepository;
	
	
	@Before
	public void setUp() throws Exception {
	}
	
	
	@After
	public void tearDown() throws Exception {
	}
	
	
	@Test
	public void findByEmail() throws Exception {
		entityManager.persist(new User("TEST_USERNAME", "TEST_PASSWORD", "TEST@EXAMPLE.COM", false));
		User u = userRepository.findByEmail("TEST@EXAMPLE.COM");
		assertEquals("Failed to find user by email", u.getUsername(), "TEST_USERNAME");
	}
	
	
	@Test
	public void findById() throws Exception {
		Long id = (Long)entityManager.persistAndGetId(new User("TEST_USERNAME", "TEST_PASSWORD", "TEST@EXAMPLE.COM", false));
		User u = userRepository.findById(id);
		assertEquals("Failed to find user by id", u.getUsername(), "TEST_USERNAME");
	}
	
	
	@Test
	public void findByUsername() throws Exception {
		entityManager.persist(new User("TEST_USERNAME", "TEST_PASSWORD", "TEST@EXAMPLE.COM", false));
		User u = userRepository.findByUsername("TEST_USERNAME");
		assertEquals("Failed to find user by username", u.getUsername(), "TEST_USERNAME");
	}
	
	
	@Test
	public void deleteByUsername() throws Exception {
		entityManager.persist(new User("TEST_USERNAME", "TEST_PASSWORD", "TEST@EXAMPLE.COM", false));
		assertEquals("Failed to create", userRepository.findByUsername("TEST_USERNAME").getUsername(), "TEST_USERNAME");
		userRepository.deleteByUsername("TEST_USERNAME");
		assertEquals("Failed to delete test user", userRepository.findByUsername("TEST_USERNAME"), null);
	}

	public void getUsersUsername() throws Exception {
		entityManager.persist(new User("TEST_USERNAME", "TEST_PASSWORD", "TEST@EXAMPLE.COM", false));
		ArrayList<String> listNom = (ArrayList<String>)(List<?>) userRepository.getUsersUsername();
		assertEquals("Failed to get usernames list", listNom.get(0), "TEST_USERNAME");
	}
	
}