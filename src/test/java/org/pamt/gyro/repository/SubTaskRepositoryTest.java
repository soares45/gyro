package org.pamt.gyro.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.pamt.gyro.bean.Status;
import org.pamt.gyro.bean.Story;
import org.pamt.gyro.bean.SubTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SubTaskRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    SubTaskRepository subTaskRepository;

    @Autowired
    private StoryRepository storyRepository;




    @Test
    public void findAllByStory(){
        Story story = new Story();
        story.setTitle("STORY_TITLE");
        story.setDescription("STORY_DESCRIPTION");
        storyRepository.save(story);
        SubTask subTask1 = new SubTask();
        subTask1.setTitle("SUBTASK_TITLE");
        subTask1.setStory(story);
        SubTask subTask2 = new SubTask();
        subTask2.setTitle("SUBTASK_2_TITLE");
        subTask2.setStory(story);
        subTaskRepository.save(subTask1);
        subTaskRepository.save(subTask2);
        List<SubTask> listeResult = subTaskRepository.findAllByStory(story);
        for (int i =0; i< listeResult.size(); i++)
            assertEquals("Failed to find subTask by story", story, listeResult.get(i).getStory());
    }

    @Test
    public void findBySubTaskId(){
        SubTask subTask = new SubTask();
        subTask.setTitle("SUBTASK_TITLE");
        subTask.setStatus(Status.TODO);
        entityManager.persist(subTask);
        SubTask subTaskFound = subTaskRepository.findBySubTaskId(subTask.getSubTaskId());
        assertEquals("Failed to find subTask by subTaskId", subTaskFound, subTask);
    }
}
