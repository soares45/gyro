package org.pamt.gyro.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.pamt.gyro.bean.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProjectRepositoryTest {
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private ProjectRepository projectRepository;
	
	
	@Test
	public void findAll() throws Exception {
	}
	
	
	@Test
	public void findAllByTitle() throws Exception {
		Project project = new Project();
		project.setTitle("PROJECT_REPOSITORY_TEST_TITLE");
		projectRepository.save(project);
		List<Project> projects = projectRepository.findAllByTitle("PROJECT_REPOSITORY_TEST_TITLE");
		System.out.println(project.getProjectId());
		System.out.println(projects.size());
		assertEquals("Failed to find all projects by title", project.getProjectId(), projects.get(0).getProjectId());
		
	}
	
	
	@Test
	public void findByProjectId() throws Exception {
		Project project = new Project();
		project.setTitle("PROJECT_TITLE");
	}
	
}