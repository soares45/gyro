package org.pamt.gyro.controller;

import com.google.gson.GsonBuilder;
import org.pamt.gyro.bean.Status;
import org.pamt.gyro.bean.User;
import org.pamt.gyro.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Map;

// TODO: changer pour url de l'application react
@CrossOrigin(origins = {"http://127.0.0.1:3030", "http://localhost:3030"})
@RestController
public class GyroController {
	
	@Autowired private UserService userService;
	@Autowired private ProjectService projectService;
	@Autowired private StoryService storyService;
	@Autowired private EpicService epicService;
	@Autowired private SubTaskService subTaskService;
	
	
	public GyroController() {}
	
	
	@GetMapping("/test")
	public String home() {
		return "Spring boot server is working";
	}
	
	
	@RequestMapping(value = "/register", method = {RequestMethod.GET, RequestMethod.POST})
	public String register(@RequestParam Map<String, String> params) {
		return userService.addUser(params.get("username"), params.get("password"), params.get("email"));
	}
	
	
	@RequestMapping(value = "/login", method = {RequestMethod.GET, RequestMethod.POST})
	public String login(@RequestParam Map<String,String> params, HttpSession session) {
		System.out.println(session.getId());
		return userService.login(params.get("username"), params.get("password"), session);
	}
	
	
	@RequestMapping(value = "/logout", method = {RequestMethod.GET, RequestMethod.POST})
	public void logout(HttpSession session) {
		session.invalidate();
	}
	
	
	@RequestMapping(value = "/checkIfUserLoggedIn", method = {RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	@Transactional(readOnly = true)
	public String checkIfUserLoggedIn(HttpSession session) {
		return userService.checkIfUserLoggedIn(session);
	}
	
	
	@RequestMapping(value = "/createProject", method = {RequestMethod.GET, RequestMethod.POST})
	public String createProject(@RequestParam Map<String,String> params, HttpSession session) {
		return projectService.createProject(getUser(session), params.get("title"), params.get("description"));
	}

	
	@RequestMapping(value = "/getProjects", method = {RequestMethod.GET, RequestMethod.POST})
	public String getProjects(HttpSession session) {
		return projectService.getProjectsByUser(getUser(session));
	}

	
	@RequestMapping(value = "/createStory", method = {RequestMethod.GET, RequestMethod.POST})
	public String createStory(@RequestParam Map<String,String> params, HttpSession session) {
		return storyService.createStory(getUser(session),
				params.get("title"),
				params.get("description"),
				Long.parseLong(params.get("projectId")));
	}

	
	@RequestMapping(value = "/createEpic", method = {RequestMethod.GET, RequestMethod.POST})
	public String createEpic(@RequestParam Map<String,String> params, HttpSession session) {
		return epicService.createEpic(getUser(session),
				params.get("title"),
				params.get("description"),
				Long.parseLong(params.get("projectId")));
	}

	
	@RequestMapping(value = "/getProject", method = {RequestMethod.GET, RequestMethod.POST})
	public String getProject(@RequestParam Map<String,String> params) {
		return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(
			  projectService.getProjectById(Long.parseLong(params.get("id"))));
	}

	
	@RequestMapping(value = "/getUsernames", method = {RequestMethod.GET, RequestMethod.POST})
	public String getUsernames() {
		return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(userService.getUsersUsername());
	}

	
	@RequestMapping(value = "/addUserToProject", method = {RequestMethod.GET, RequestMethod.POST})
	public String addUserToProject(@RequestParam Map<String,String> params, HttpSession session) {
		return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(projectService.addMemberToProject
				(getUser(session), params.get("username"), Long.parseLong(params.get("projectId"))));
	}
	
	
	@RequestMapping(value = "/deleteProject", method = {RequestMethod.GET, RequestMethod.POST})
	public String deleteProject(@RequestParam Map<String,String> params, HttpSession session) {
		return projectService.deleteProject(Long.parseLong(params.get("id")), getUser(session));
	}


	@RequestMapping(value = "/getEpicsByProjectId", method = {RequestMethod.GET, RequestMethod.POST})
	public String getEpicsByProjectId(@RequestParam Map<String,String> params) {
		return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(epicService
				.getEpicsByProjectId(Long.parseLong(params.get
				("projectId"))));
	}


	@RequestMapping(value = "/addStoryToEpic", method = {RequestMethod.GET, RequestMethod.POST})
	public String addStoryToEpic(@RequestParam Map<String,String> params) {
		return epicService.addStoryToEpic(Long.parseLong(params.get("epicId")), Long.parseLong(params.get
				("storyId")));
	}


	@RequestMapping(value = "/createSubTask", method = {RequestMethod.GET, RequestMethod.POST})
	public String createSubTask(@RequestParam Map<String,String> params) {
		return subTaskService.createSubTask(Long.parseLong(params.get("storyId")), params.get("title"));
	}

	
	@RequestMapping(value = "/assignUserToStory", method = {RequestMethod.GET, RequestMethod.POST})
	public String assignUserToStory(@RequestParam String username, @RequestParam Long storyId) {
		return storyService.assignUser(username, storyId);
	}

	
	@RequestMapping(value = "/unassignStory", method = {RequestMethod.GET, RequestMethod.POST})
	public String unassignStory(@RequestParam Long storyId) {
		return storyService.unassignStory(storyId);
	}

	
	@RequestMapping(value = "/updateStory", method = {RequestMethod.GET, RequestMethod.POST})
	public String updateStory(@RequestParam Map<String,String> params, HttpSession session) {
		return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(
			  storyService.updateStory(getUser(session),
			  	    Long.parseLong(params.get("storyId")),
				    params.get("title"),
				    params.get("desc"),
				    Integer.parseInt(params.get("points"))));
	}

	
	@RequestMapping(value = "/updateProject", method = {RequestMethod.GET, RequestMethod.POST})
	public String updateProject(@RequestParam Map<String,String> params, HttpSession session) {
		return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(
				projectService.updateProject(getUser(session),
						Long.parseLong(params.get("projectId")),
						params.get("title"),
						params.get("desc")));
	}

	
	@RequestMapping(value = "/updateSubTaskStatus", method = {RequestMethod.GET, RequestMethod.POST})
	public String updateSubTaskStatus(@RequestParam Map<String,String> params) {
		return subTaskService.updateSubTaskStatus(Long.parseLong(params.get("subTaskId")), Status.valueOf(params.get("status")));
	}

	
	@RequestMapping(value = "/updateSubTask", method = {RequestMethod.GET, RequestMethod.POST})
	public String updateSubTask(@RequestParam Long subTaskId, @RequestParam String title) {
		return subTaskService.updateSubTask(subTaskId, title);
	}

	
	@RequestMapping(value = "/deleteSubTask", method = {RequestMethod.GET, RequestMethod.POST})
	public String deleteSubTask(@RequestParam Long subTaskId) {
		return subTaskService.deleteSubTask(subTaskId);
	}
	
	
	@RequestMapping(value = "/deleteStory", method = {RequestMethod.GET, RequestMethod.POST})
	public String deleteStory(@RequestParam Long storyId, HttpSession session) {
		return storyService.deleteStory(storyId, getUser(session));
	}

	
	@RequestMapping(value = "/getOwnedProjects", method = {RequestMethod.GET, RequestMethod.POST})
	public String getOwnedProjects(HttpSession session) {
		return projectService.getOwnedProjectsByUser(getUser(session));
	}

	@RequestMapping(value = "/deleteEpic", method = {RequestMethod.GET, RequestMethod.POST})
	public String deleteEpic(@RequestParam Long epicId, HttpSession session) {
		return epicService.deleteEpic(epicId, getUser(session));
	}
	
	////////////////////////
	// NON-MAPPED METHODS //
	////////////////////////
	
	
	private User getUser(HttpSession session) {
		return (User)session.getAttribute("USER");
	}
	
}
