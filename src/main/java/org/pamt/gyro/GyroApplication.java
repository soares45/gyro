package org.pamt.gyro;

import org.pamt.gyro.dao.DAO;
import org.pamt.gyro.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import javax.servlet.ServletContextListener;


@SpringBootApplication
@ComponentScan
public class GyroApplication {

    @Autowired DemoService demoService;
	
    @Value("${spring.datasource.driver-class-name}")
    private String currentSQLDriver;
    

    public static void main(String[] args) {
	    SpringApplication.run(GyroApplication.class, args);
	}

	
    @Bean
    public CommandLineRunner initTestDemo(DemoService demoService) {
	    if (currentSQLDriver.equalsIgnoreCase("com.mysql.jdbc.Driver")) {
		    DAO.ENVIRONMENT = DAO.Environment.PROD;
	    }
        return (args) -> {
            if (DAO.ENVIRONMENT == DAO.Environment.DEV) {
                demoService.demoGyro();
            } else {
            	System.out.println("Not running demoService because connected to PROD database");
            }
        };
    }

}
