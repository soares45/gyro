package org.pamt.gyro.bean;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;


@Entity
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Expose private Long id;

	@Expose private String username;
	@Expose private String email;
	@Expose private boolean isAdmin;
	private String password;
	
	
	public User(String username, String password, String email, boolean isAdmin) {
		this.username = username;
		this.password = password;
		this.email = email;
		this.isAdmin = isAdmin;
	}
	
	protected User() {}
	
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	public boolean isAdmin() {
		return isAdmin;
	}
	
	
	public void setAdmin(boolean admin) {
		isAdmin = admin;
	}


	public String getUsername() {
		return username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getEmail() {
		return email;
	}
	
	
	public Long getId() {
		return id;
	}
	
	
	public void setId(Long id) {
		this.id = id;
	}
	
}
