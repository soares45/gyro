package org.pamt.gyro.bean;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Story implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Expose private Long storyId;
	
	@ManyToOne
	@JoinColumn(name = "projectId")
	private Project project;
	
	@OneToMany(mappedBy = "story", cascade = {CascadeType.ALL})
	@Expose private List<SubTask> subtasks;
	
	@ManyToOne
	@JoinColumn(name = "epicId")
	@Expose private Epic epic;
	
	@Expose private String title;
	@Expose private String description;
	@Expose private Status status;
	@Expose private int points;
	@Expose private String assigneeUsername;
	
	
	public Story(Long storyId, Project project, List<SubTask> subtasks, String title, String description, Status status, int points, String assigneeUsername) {
		this.storyId = storyId;
		this.project = project;
		this.subtasks = subtasks;
		this.title = title;
		this.description = description;
		this.status = status;
		this.points = points;
		this.assigneeUsername = assigneeUsername;
	}
	
	
	public Story(Project project, String title, String description, Status status, int points, String assigneeUsername) {
		this.project = project;
		this.subtasks = new ArrayList<>();
		this.title = title;
		this.description = description;
		this.status = status;
		this.points = points;
		this.assigneeUsername = assigneeUsername;
	}
	
	
	// TODO : Remove after end of debug moment
    public Story(Project project, String title, String assigneeUsername) {
        this.project = project;
        this.title = title;
        this.assigneeUsername = assigneeUsername;
        this.subtasks = new ArrayList<SubTask>();
    }

	public Story() {
		this.subtasks = new ArrayList<SubTask>();
	}
	
	
	public Long getStoryId() {
		return storyId;
	}
	
	
	public void setStoryId(Long storyId) {
		this.storyId = storyId;
	}
	
	
	public Project getProject() {
		return project;
	}
	
	
	public void setProject(Project project) {
		this.project = project;
	}
	
	
	public List<SubTask> getSubtasks() {
		return subtasks;
	}
	
	
	public void setSubtasks(List<SubTask> subtasks) {
		this.subtasks = subtasks;
	}
	
	
	public String getTitle() {
		return title;
	}
	
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public String getDescription() {
		return description;
	}
	
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public Status getStatus() {
		return status;
	}
	
	
	public void setStatus(Status status) {
		this.status = status;
	}
	
	
	public int getPoints() {
		return points;
	}
	
	
	public void setPoints(int points) {
		this.points = points;
	}
	
	
	public String getAssigneeUsername() {
		return assigneeUsername;
	}
	
	
	public void setAssigneeUsername(String assigneeUsername) {
		this.assigneeUsername = assigneeUsername;
	}
	
	
	public Epic getEpic() {
		return epic;
	}
	
	
	public void setEpic(Epic epic) {
		this.epic = epic;
	}
}
