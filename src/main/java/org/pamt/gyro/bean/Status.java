package org.pamt.gyro.bean;


public enum Status {
	TODO,
	IN_PROGRESS,
	DONE;
}
