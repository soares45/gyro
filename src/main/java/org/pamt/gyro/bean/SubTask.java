package org.pamt.gyro.bean;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;


@Entity
public class SubTask implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Expose private Long subTaskId;
	
	@Expose private String title;
	@Expose private Status status;
	
	@ManyToOne(cascade = {CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name = "storyId")
	private Story story;
	
	
	public SubTask(String title, Status status, Story story) {
		this.title = title;
		this.status = status;
		this.story = story;
	}
	
	
	public SubTask() {}


	public String getTitle() {
		return title;
	}


	public Status getStatus() {
		return status;
	}
	
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public void setStatus(Status status) {
		this.status = status;
	}
	
	
	public Story getStory() {
		return story;
	}
	
	
	public void setStory(Story story) {
		this.story = story;
	}
	
	
	public Long getSubTaskId() {
		return subTaskId;
	}
	
	
	public void setSubTaskId(Long subTaskId) {
		this.subTaskId = subTaskId;
	}
}
