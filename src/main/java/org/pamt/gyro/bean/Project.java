package org.pamt.gyro.bean;

import com.google.gson.annotations.Expose;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Project implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Expose private Long projectId;
	
	@ElementCollection(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	@Expose private List<String> memberUsernames;

	@OneToMany(mappedBy = "project", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	@Expose private List<Story> stories;
	
	@OneToMany(mappedBy = "project", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	@Expose private List<Epic> epics;
	
	@Expose private String title;
	@Expose private String description;
	@Expose private String ownerUsername;
	

	public Project(Long projectId, String title, String description, String ownerUsername, List<Story> stories) {
		this(title, description, ownerUsername, stories);
		this.projectId = projectId;
	}
	
	
	public Project(String title, String description, String ownerUsername, List<Story> stories) {
		this(title, description, ownerUsername);
		this.stories = stories;
	}
	
	
	public Project(String title, String description, String ownerUsername) {
		this();
		this.title = title;
		this.description = description;
		this.ownerUsername = ownerUsername;
	}
	
	
	public Project() {
		this.memberUsernames = new ArrayList<>();
		this.stories = new ArrayList<>();
		this.epics = new ArrayList<>();
	}
	
	
	public Long getProjectId() {
		return projectId;
	}
	
	
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	
	
	public String getTitle() {
		return title;
	}
	
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public String getDescription() {
		return description;
	}
	
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public String getOwnerUsername() {
		return ownerUsername;
	}
	
	
	public void setOwnerUsername(String ownerUsername) {
		this.ownerUsername = ownerUsername;
	}
	
	
	public List<Story> getStories() {
		return stories;
	}
	
	
	public void setStories(List<Story> stories) {
		this.stories = stories;
	}

	
    public void addStory(Story story) {
	    stories.add(story);
    }

    
	public List<Epic> getEpics() {
		return epics;
	}

	
	public void setEpics(List<Epic> epics) {
		this.epics = epics;
	}
	
	
	public List<String> getMemberUsernames() {
		return memberUsernames == null ? new ArrayList<>() : memberUsernames;
	}
	
	
	public void setMemberUsernames(List<String> memberUsernames) {
		this.memberUsernames = memberUsernames;
	}
	
	
	public boolean addMember(String memberUsername) {
		if (!memberUsernames.contains(memberUsername) && !memberUsername.equalsIgnoreCase(ownerUsername)) {
			this.memberUsernames.add(memberUsername);
			return true;
		}
		return false;
	}
}
