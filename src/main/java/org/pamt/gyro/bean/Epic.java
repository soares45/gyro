package org.pamt.gyro.bean;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * In Gyro, an Epic is a folder of Stories.
 */
@Entity
public class Epic implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Expose private Long epicId;
	
	@ManyToOne
	@JoinColumn(name = "projectId")
	private Project project;
	
	@Expose private String title;
	@Expose private String description;
	@Expose private Status status;
	@Expose private int points;
	@Expose private String assigneeUsername;

	@OneToMany(mappedBy = "epic", cascade = {CascadeType.REMOVE, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH}, fetch = FetchType.EAGER)
	private List<Story> stories;

	public Epic() {
		stories = new ArrayList<Story>();
	}
	
	public Long getEpicId() {
		return epicId;
	}
	
	
	public void setEpicId(Long epicId) {
		this.epicId = epicId;
	}
	
	public String getTitle() {
		return title;
	}
	
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public String getDescription() {
		return description;
	}
	
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public Status getStatus() {
		return status;
	}
	
	
	public void setStatus(Status status) {
		this.status = status;
	}
	
	
	public int getPoints() {
		return points;
	}
	
	
	public void setPoints(int points) {
		this.points = points;
	}
	
	
	public String getAssigneeUsername() {
		return assigneeUsername;
	}
	
	
	public void setAssigneeUsername(String assigneeUsername) {
		this.assigneeUsername = assigneeUsername;
	}
	
	
	public Project getProject() {
		return project;
	}
	
	
	public void setProject(Project project) {
		this.project = project;
	}

	public List<Story> getStories() {
		return stories;
	}

	public void setStories(List<Story> stories) {
		this.stories = stories;
	}
}
