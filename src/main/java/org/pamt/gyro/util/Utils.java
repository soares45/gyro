package org.pamt.gyro.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;


public interface Utils {
	
	static String jsonReply(Object... o) {
		Map<Object, Object> msg = new HashMap<Object, Object>();
		if (!((o.length & 1) == 1)) {
			for (int i = 1; i < o.length; i += 2) {
				msg.put(o[i - 1], o[i]);
			}
		} else {
			System.out.println("BAD NUMBER OF ARGUMENT INSERTED. MUST BE EVEN. TODO: EXCEPTION INSTEAD OF SYSOUT");
		}
		return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(msg);
	}
	
	
	static boolean checkIfJSONReplyHasErrorAsTrue(String jsonReply) {
		JsonObject json = new Gson().fromJson(jsonReply, JsonObject.class);
		return Boolean.parseBoolean(json.get("ERROR").toString());
	}
	
}
