package org.pamt.gyro.util;

import java.security.MessageDigest;
import java.util.Base64;

public interface HashUtils {
	
	static String sha256(String password){
		String result = "";
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] passwordBytes = password.getBytes("UTF-8");
			byte[] passwordBytesArray = digest.digest(passwordBytes);
			result = Base64.getEncoder().encodeToString(passwordBytesArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
}
