package org.pamt.gyro.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;


public interface FileUtils {
	
	/**
	 * Writes data a file
	 * @param data Data to put in file
	 * @param fName File name
	 * @param append Appends to the end of the file or not
	 * @return Whether the file exists or not
	 */
	static boolean writeInFile(String data, String fName, boolean append) {
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fName, append)));
			out.println(data);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return (new File(fName).exists());
	}
	
	
	/**
	 * Puts the content of a file in a String
	 * @param fullPath Path to the file 
	 * @return Content of the file
	 */
	static String getFileContents(String fullPath) {
		String contents = "";
		try {
			contents = new String(Files.readAllBytes(Paths.get(fullPath)));
		} catch (IOException e) {
			System.out.println("Unable to convert file to string: " + e.getMessage());
		}
		return contents;
	}
}
