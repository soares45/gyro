package org.pamt.gyro.service;

import org.pamt.gyro.bean.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class DemoService {
	
	@Autowired private ProjectService  projectService;
	@Autowired private UserService     userService;
	@Autowired private StoryService    storyService;
	@Autowired private EpicService     epicService;
	@Autowired private SubTaskService  subTaskService;
	
	
	public void demoGyro() {
		demoUser();
		demoProject();
		demoEpic();
		demoStory();
		demoSubTask();
	}
	
	
	private void demoUser() {
		userService.addUser("test", "testtest", "test@test.com");
		userService.addUser("johndoe", "password", "johndoe@example.com");
		userService.addUser("cathy", "isverycute", "cathyisso@cute.com");
		userService.addUser("donaldtrump", "password", "donaldtrump@wallbuilder.com");
		userService.addUser("root", "rootroot", "root@root.com", true); // admin
		
		for (User u : userService.getUsers()) {
			System.out.println("id=" + u.getId() + "; username=" + u.getUsername() + ";");
		}
	}
	
	
	private void demoProject() {
		projectService.createProject(userService.getUserByUsername("test"), "Project title here", "Nulla vel eros purus. Sed enim lacus, cursus sed est id, dapibus molestie orci.");
		projectService.createProject(userService.getUserByUsername("test"), "Gyro", "The Gyro experience");
		
		for (Project p : projectService.getProjects()) {
			System.out.println("projectId=" + p.getProjectId() + "; title=" + p.getTitle() + ";");
		}
	}
	
	
	private void demoEpic() {
		User user = userService.getUserByUsername("test");
		epicService.createEpic(user, "Epic A", "Enter epic A description here", 1L);
		
		for (Epic epic : epicService.getEpicsByProjectId(1L)) {
			System.out.println("epicId=" + epic.getEpicId() + "; title=" + epic.getTitle() + ";");
		}
	}
	
	
	private void demoStory() {
		User user = userService.getUserByUsername("test");
		storyService.createStory(user, "Story A", "Enter story A description here", 1L);
		storyService.createStory(user, "Story B", "Enter story B description here", 1L);
		storyService.createStory(user, "Story C", "Enter story C description here", 1L);
		storyService.createStory(user, "Story D", "Enter story D description here", 1L);
		storyService.createStory(user, "Story E", "Enter story E description here", 1L);
		storyService.createStory(user, "Story F", "Enter story F description here", 1L);
		
		for (Story s : storyService.getStoriesByProjectId(1L)) {
			System.out.println("storyId=" + s.getStoryId() + "; title=" + s.getTitle() + ";");
		}
	}
	
	
	private void demoSubTask() {
		subTaskService.createSubTask(1L, "Clean mess made by teammates");
		subTaskService.createSubTask(2L, "Fix this mess");
		subTaskService.createSubTask(1L, "Fix it all");
		subTaskService.createSubTask(2L, "Set port to 65536");
		
		for (SubTask s : subTaskService.getSubTasksByStoryId(1L)) {
			System.out.println("subTaskId=" + s.getSubTaskId() + "; title=" + s.getTitle() + ";");
		}
		for (SubTask s : subTaskService.getSubTasksByStoryId(2L)) {
			System.out.println("subTaskId=" + s.getSubTaskId() + "; title=" + s.getTitle() + ";");
		}
	}
}
