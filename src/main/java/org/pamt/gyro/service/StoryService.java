package org.pamt.gyro.service;

import org.hibernate.Hibernate;
import org.pamt.gyro.bean.Project;
import org.pamt.gyro.bean.Status;
import org.pamt.gyro.bean.Story;
import org.pamt.gyro.bean.User;
import org.pamt.gyro.repository.ProjectRepository;
import org.pamt.gyro.repository.StoryRepository;
import org.pamt.gyro.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpSession;
import java.util.List;

@org.springframework.stereotype.Service
public class StoryService extends Service {

	private static final int STORY_TITLE_MIN_LENGTH = 4;
	
	@Autowired private StoryRepository storyRepository;
	@Autowired private ProjectRepository projectRepository;
	@Autowired private UserService userService;
	
	
	public StoryService() {}
	
	
	public String createStory(User user, String title, String description, Long projectId) {
		if (user == null) {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "User must be logged in to create a story");
		}
		Project project = projectRepository.findByProjectId(projectId);
		if (userCanAddOrUpdateStoryInProject(user, project)) {
			if (!validateStoryTitle(title)) {
				return Utils.jsonReply("ERROR", true, "MESSAGE", "story title must be at least "
					  + STORY_TITLE_MIN_LENGTH + " characters");
			}
			Story story = new Story();
			story.setTitle(title);
			story.setDescription(description);
			story.setStatus(Status.TODO);
			story.setProject(project);
			Story result = storyRepository.save(story);
			System.out.println(result.getStoryId());
			return Utils.jsonReply("ERROR", false, "MESSAGE", "Story created successfully", "ID", story.getStoryId());
		} else {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "You do not have the sufficent permissions to add a story to this project");
		}
	}

	
	private boolean validateStoryTitle(String title) {
		return title.length() >= STORY_TITLE_MIN_LENGTH;
	}
	
	
	public String deleteStory(Long id, User user) {
		Story story = getStoryById(id);
		Project project = story.getProject();
		if (userCanAddOrUpdateStoryInProject(user, project)) {
			project.getStories().remove(story); // delete the story from the project
			projectRepository.save(project); // save the project (without the story being deleted)
			storyRepository.delete(id); // delete the story
			return Utils.jsonReply("ERROR", false, "MESSAGE", "Story has been deleted");
		}
		return Utils.jsonReply("ERROR", true, "MESSAGE", "You cannot delete this story.");
	}
	
	
	public Story getStoryById(Long id) {
		return storyRepository.findByStoryId(id);
	}

	
	/**
	 * Finds and returns all stories for a given project Id
	 * @param projectId  Id of the project
	 * @return List of stories
	 */
	public List<Story> getStoriesByProjectId(Long projectId) {
		Project project = projectRepository.findByProjectId(projectId);
		return storyRepository.findAllByProject(project);
	}

	
	public String assignUser(String username, Long storyId) {
		if (!userService.userExists(username)) {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "User " + username + " doesn't exist");
		}
		Story story = this.getStoryById(storyId);
		if (story == null) {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "Story with id " + storyId + " doesn't exist");
		}
		story.setAssigneeUsername(username);
		storyRepository.save(story);
		return Utils.jsonReply("ERROR", false, "MESSAGE", username + " has been successfully assigned to the story " + story.getTitle());
	}

	
    public String unassignStory(Long storyId) {
		Story story = this.getStoryById(storyId);
		if(story == null) {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "Story with id " + storyId + " doesn't exist");
		}
		story.setAssigneeUsername(null);
		storyRepository.save(story);
		return Utils.jsonReply("ERROR", false, "MESSAGE", "The story " + story.getTitle() + " has been successfully unassigned");
    }

	
	public String updateStory(User user, long storyId, String title, String desc, int points) {
		if (user == null) {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "User must be logged in to update a story");
		}
		Story story = getStoryById(storyId);
		if (userCanAddOrUpdateStoryInProject(user, story.getProject())) {
			story.setTitle(title);
			story.setDescription(desc);
			story.setPoints(points);
			storyRepository.save(story);
			return Utils.jsonReply("ERROR", false, "MESSAGE", "Story has been updated");
		} else {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "You do not have the sufficent permissions to update this story");
		}
	}

	public void removeEpic(Story story) {
		story.setEpic(null);
		storyRepository.save(story);
	}

	private boolean userCanAddOrUpdateStoryInProject(User user, Project project) {
		return (user != null && (user.isAdmin()
			  || project.getMemberUsernames().contains(user.getUsername())
			  || project.getOwnerUsername().equalsIgnoreCase(user.getUsername())));
	}
}
