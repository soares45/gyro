package org.pamt.gyro.service;

import com.google.gson.Gson;
import org.pamt.gyro.bean.Epic;
import org.pamt.gyro.bean.Project;
import org.pamt.gyro.bean.Story;
import org.pamt.gyro.bean.User;
import org.pamt.gyro.repository.ProjectRepository;
import org.pamt.gyro.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpSession;
import java.util.*;


@org.springframework.stereotype.Service
public class ProjectService extends Service {

	@Autowired
	ProjectRepository projectRepository;


	@Autowired
	private UserService userService;


	public ProjectService() {}


	/**
	 * Creates a project
	 *
	 * @param user        Username trying to create the project, and also the product owner of the project being created
	 * @param title
	 * @param description
	 * @return
	 */
	public String createProject(User user, String title, String description) {
		if (user == null) {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "User must be logged in to create a project");
		}
		Project project = new Project();
		//project.setProjectId(getCurrentId());
		project.setTitle(title);
		project.setDescription(description);
		project.setOwnerUsername(user.getUsername());
		projectRepository.save(project);
		//incrementCurrentId();
		// TODO: do not return "ID" with "PROJECT", just return "PROJECT" (as well as "ERROR" and "MESSAGE".)
		// I'm not doing it right now because I'd have to modify the tests.
		return Utils.jsonReply("ERROR", false, "MESSAGE", "Project created successfully", "ID", project.getProjectId(),
			  "PROJECT", new Gson().toJson(project));
	}


	public String deleteProject(Long id, User user) {
		if (user == null) {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "User must be logged in to delete a project.");
		}
		Project p = projectRepository.findByProjectId(id);
		if (p.getOwnerUsername().equalsIgnoreCase(user.getUsername()) || user.isAdmin()) {
			projectRepository.delete(id);
			return Utils.jsonReply("ERROR", false, "MESSAGE", p.getTitle()+" has been deleted successfully.");
		} else {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "You must be the owner of this project to delete it.");
		}
	}


	public Project getProjectById(Long id) {
		return projectRepository.findByProjectId(id);
	}


	public List<Project> getProjectsByTitle(String title) {
		return projectRepository.findAllByTitle(title);
	}


	public List<Project> getProjects() {
		return projectRepository.findAll();
	}


	public String addMemberToProject(User user, String memberUsername, Long projectId) {
		Project project = getProjectById(projectId);
		if (!user.getUsername().equalsIgnoreCase(project.getOwnerUsername())) {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "You must be the project owner to add a member to a group");
		} else if (userService.getUserByUsername(memberUsername) == null) {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "User '" + memberUsername + "' does not exist.");
		} else if (project.addMember(memberUsername)) {
			return Utils.jsonReply("ERROR", false, "MESSAGE", memberUsername + " has been added to the project");
		} else {
			return Utils.jsonReply("ERROR", true, "MESSAGE", memberUsername + " is already part of your project!");
		}
	}

	public String getProjectsByUser(User user) {
		if (user == null) {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "User must be logged in to get its projects");
		}
		List<Project> projects;
		String username = user.getUsername();
		if (user.isAdmin()) {
			projects = projectRepository.findAll();
		} else {
			projects = projectRepository.findAllByOwnerUsernameOrMemberUsernames(username, Collections.singletonList(username));
		}
		return Utils.jsonReply("ERROR", false, "PROJECTS", new HashSet<Project>(projects));
	}

	public String updateProject(User user, long storyId, String title, String desc) {
		if (user == null) {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "User must be logged in to update a project");
		}
		Project project = getProjectById(storyId);
		if (userCanAddOrUpdateProject(user, project)) {
			project.setTitle(title);
			project.setDescription(desc);
			projectRepository.save(project);
			return Utils.jsonReply("ERROR", false, "MESSAGE", "Project has been updated");
		} else {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "You do not have the sufficent permissions to update this project");
		}
	}

	private boolean userCanAddOrUpdateProject(User user, Project project) {
		System.out.println(project);

		return (user != null && (user.isAdmin()
				|| project.getMemberUsernames().contains(user.getUsername())
				|| project.getOwnerUsername().equalsIgnoreCase(user.getUsername())));
	}

	public String getOwnedProjectsByUser(User user) {
		if (user == null) {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "User must be logged in to get its projects");
		}
		List<Project> projects;
		String username = user.getUsername();
		if (user.isAdmin()) {
			projects = projectRepository.findAll();
		} else {
			projects = projectRepository.findAllByOwnerUsername(username);
		}
		return Utils.jsonReply("ERROR", false, "PROJECTS", projects);
	}

	public void removeEpic(Project project, Epic epic) {
		project.getEpics().remove(epic);
		projectRepository.save(project);
	}
}

