package org.pamt.gyro.service;

import java.util.ArrayList;
import java.util.List;

import org.pamt.gyro.bean.Project;
import org.pamt.gyro.bean.User;
import org.pamt.gyro.repository.UserRepository;
import org.pamt.gyro.util.HashUtils;

import com.google.gson.Gson;
import org.pamt.gyro.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpSession;


@org.springframework.stereotype.Service
public class UserService extends Service {
	
	public static final int MAX_USERNAME_LENGTH = 32;
	public static final int MIN_USERNAME_LENGTH = 4;
	public static final int MAX_EMAIL_LENGTH = 255;
	public static final int MIN_PASSWORD_LENGTH = 8;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProjectService projectService;
	

	public UserService() {}
	

	/**
	 * Gets all users in a JSON format
	 * @return User in JSON
	 */
	public String usersToJson() {
		return new Gson().toJson(userRepository.findAll());
	}
	
	
	/**
	 * Gets the user by its username
	 * @param username
	 * @return The user
	 */
	public User getUserByUsername(String username) {
		System.out.println("Trying to search username "+username);
		return userRepository.findByUsername(username);
	}
	
	
	public User getUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	public User getUserById(Long id){
		return userRepository.findById(id);
	}
	
	
	public List<User> getUsers() {
		return userRepository.findAll();
	}
	
	
	public List<String> getUsersUsername() {
		return (ArrayList<String>)(List<?>) userRepository.getUsersUsername();
	}
	
	
	public String addUser(String username, String password, String email, boolean isAdmin) {
		if (userRepository.countByUsername(username) != 0) {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "Username is already taken");
		}
		User user = new User(username, password, email, isAdmin);
		try {
			verifyUser(user);
			user.setPassword(HashUtils.sha256(password));
			userRepository.save(user);
			return Utils.jsonReply("ERROR", false, "MESSAGE", "Registered successfully");
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("[addUser] ERROR: "+e.getMessage());
			return Utils.jsonReply("ERROR", true, "MESSAGE", e.getMessage());
		}
	}
	
	
	public String addUser(String username, String password, String email) {
		return addUser(username, password, email, false);
	}
	
	
	// TODO: make a class dedicated to verification (verifyNewUser, verifyNewRequest, etc...)
	private void verifyUser(User user) throws Exception {
		if (user.getUsername().length() > MAX_USERNAME_LENGTH) {
			throw new Exception("Username must be shorter or equal to " + MAX_USERNAME_LENGTH);
		} else if (user.getUsername().length() < MIN_USERNAME_LENGTH) {
			throw new Exception("Username must be longer or equal to " + MIN_USERNAME_LENGTH);
		} else if (user.getEmail().length() > MAX_EMAIL_LENGTH) {
			throw new Exception("Email must be shorter or equal to " + MAX_EMAIL_LENGTH);
		} else if (getUserByUsername(user.getUsername()) != null) {
			throw new Exception("Username is already taken");
		} else if (getUserByEmail(user.getEmail()) != null) {
			throw new Exception("Email is already taken");
		} else if (user.getPassword().length() < MIN_PASSWORD_LENGTH) {
			throw new Exception("Password must be " + MIN_PASSWORD_LENGTH + " or longer");
		}
	}

	
	public void deleteUser(String username) {
		//users.remove(getUserByUsername(username));
		userRepository.deleteByUsername(username);
	}
	
	
	public String login(String username, String password, HttpSession session) {
		User user = getUserByUsername(username);
		if (user == null || !user.getPassword().equals(HashUtils.sha256(password))) {
			return Utils.jsonReply("ERROR", true, "MESSAGE", "Invalid username or password."); // bad username
		} else {
			System.out.println("Saving USER to session: "+user);
			if (session != null) { // There is no reason session could be null unless we're running a test
				session.setAttribute("USER", user);
			}
			return Utils.jsonReply("ERROR", false, "USER", user);
		}
	}
	
	
	/**
	 * Checks if the user is connected (by looking through the user's session
	 * @param session
	 * @return The user data
	 */
	public String checkIfUserLoggedIn(HttpSession session) {
		System.out.println(session.getAttribute("USER"));
		System.out.println(session.getId());
		
		if ((User)session.getAttribute("USER") != null) {
			return Utils.jsonReply("ERROR", false, "USER", (User)session.getAttribute("USER"));
		}
		// user isn't connected, but since this method will be called everytime a page is refreshed, we don't return
		// any ERROR, because the user might just be a guest looking around on the website.
		return "{}";
	}
	
	
	public void createAdmin(String username, String password, String email) {
		User user = new User(username, password, email, false);
	}

	public boolean userExists(String username) {
		return this.getUserByUsername(username) != null;
	}
}
