package org.pamt.gyro.service;

import org.pamt.gyro.bean.Status;
import org.pamt.gyro.bean.Story;
import org.pamt.gyro.bean.SubTask;
import org.pamt.gyro.repository.StoryRepository;
import org.pamt.gyro.repository.SubTaskRepository;
import org.pamt.gyro.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@org.springframework.stereotype.Service
public class SubTaskService extends Service {

    private static final int SUBTASK_TITLE_MIN_LENGTH = 4;

    @Autowired private SubTaskRepository subTaskRepository;
    @Autowired private StoryRepository storyRepository;

    public SubTaskService(){}

    public String createSubTask(Long storyId, String title){
        Story story = storyRepository.findByStoryId(storyId);
        if (story == null) {
            return Utils.jsonReply("ERROR", true, "MESSAGE", "Story id invalid");
        }
        //TODO : permission
        if (!validateSubTaskTitle(title)) {
            return Utils.jsonReply("ERROR", true, "MESSAGE", "subTask title must be at least "
                    + SUBTASK_TITLE_MIN_LENGTH + " characters");
        }
        SubTask subTask = new SubTask(title, Status.TODO, story);
        subTaskRepository.save(subTask);
        return Utils.jsonReply("ERROR", false, "MESSAGE", "SubTask created successfully", "ID", subTask.getSubTaskId());
    }

    private boolean validateSubTaskTitle(String title){ return title.length() >= SUBTASK_TITLE_MIN_LENGTH; }

    public String deleteSubTask(Long subTaskId) {
        SubTask subTask = this.getSubTaskById(subTaskId);
        if(subTask == null) {
            return Utils.jsonReply("ERROR", true, "MESSAGE", "The task with ID " + subTaskId + " doesn't exist");
        }
        subTaskRepository.delete(subTaskId);
        return Utils.jsonReply("ERROR", false, "MESSAGE", "The task with ID " + subTaskId + " has been successfully deleted");
    }

    public SubTask getSubTaskById(Long id){ return subTaskRepository.findBySubTaskId(id);}

    public List<SubTask> getSubTasksByStoryId(Long storyId){
        Story story = storyRepository.findByStoryId(storyId);
        return subTaskRepository.findAllByStory(story);
    }

    public String updateSubTaskStatus(Long subTaskId, Status status) {
        SubTask subTask = this.getSubTaskById(subTaskId);
        if(subTask == null) {
            return Utils.jsonReply("ERROR", true, "MESSAGE", "The task with ID " + subTaskId + " doesn't exist");
        }
        subTask.setStatus(status);
        subTaskRepository.save(subTask);
        return Utils.jsonReply("ERROR", false, "MESSAGE", "The status of task with ID " + subTaskId + " has been successfully updated");
    }

    public String updateSubTask(Long subTaskId, String title) {
        SubTask subTask = this.getSubTaskById(subTaskId);
        if(subTask == null) {
            return Utils.jsonReply("ERROR", true, "MESSAGE", "The task with ID " + subTaskId + " doesn't exist");
        }
        else if (!validateSubTaskTitle(title)) {
            return Utils.jsonReply("ERROR", true, "MESSAGE", "subTask title must be at least "
                    + SUBTASK_TITLE_MIN_LENGTH + " characters");
        }
        subTask.setTitle(title);
        subTaskRepository.save(subTask);
        return Utils.jsonReply("ERROR", false, "MESSAGE", "The task with ID " + subTaskId + " has been successfully updated");
    }
}
