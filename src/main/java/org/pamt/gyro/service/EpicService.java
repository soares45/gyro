package org.pamt.gyro.service;

import org.pamt.gyro.bean.*;
import org.pamt.gyro.repository.EpicRepository;
import org.pamt.gyro.repository.ProjectRepository;
import org.pamt.gyro.repository.StoryRepository;
import org.pamt.gyro.repository.UserRepository;
import org.pamt.gyro.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@org.springframework.stereotype.Service
public class EpicService extends Service{

    @Autowired
    private EpicRepository epicRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private StoryRepository storyRepository;

    @Autowired
    private StoryService storyService;

    @Autowired
    private ProjectService projectService;



    public EpicService() {}
    

    public String createEpic(User user, String title, String description, Long projectId) {
        if (user == null) {
            return Utils.jsonReply("ERROR", true, "MESSAGE", "User must be logged in to create an epic");
        }
        // TODO: check if user is allowed to create EPIC in project
        Epic epic = new Epic();
        epic.setTitle(title);
        epic.setDescription(description);
        epic.setStatus(Status.TODO);
        Project project = projectRepository.findByProjectId(projectId);
        epic.setProject(project);
        Epic result = epicRepository.save(epic);
        System.out.println(result.getEpicId());

        return Utils.jsonReply("ERROR", false, "MESSAGE", "Epic created successfully", "ID", epic.getEpicId());
    }

    public String addStoryToEpic(Long epicId, Long storyId){
        Epic epic = epicRepository.findByEpicId(epicId);
        Story story = storyRepository.findByStoryId(storyId);
        if (epic == null || story == null)
            return Utils.jsonReply("ERROR", true, "MESSAGE", "Wrong epicId or storyId");
        story.setEpic(epic);
        storyRepository.save(story);
        return Utils.jsonReply("ERROR", false, "MESSAGE", "Story successfully added to the Epic", "epicId",
                epic.getEpicId(), "storyId", story.getStoryId());
    }


    public String deleteEpic(Long id, User user) {
        if(user == null) {
            return Utils.jsonReply("ERROR", true, "MESSAGE", "User must be logged in to delete an epic");
        }

        Epic epic = this.getEpicById(id);
        if(epic == null) {
            return Utils.jsonReply("ERROR", true, "MESSAGE", "Epic with id " + id + " doesn't exist");
        }

        Project project = epic.getProject();
        if(userCanAddOrUpdateEpicInProject(user, project)) {
            projectService.removeEpic(project, epic);
            removeEpicFromStories(epic);
            epicRepository.delete(id);
            return Utils.jsonReply("ERROR", false, "MESSAGE", "The epic has been successfully deleted");
        }
        return Utils.jsonReply("ERROR", true, "MESSAGE", "You cannot delete this epic.");
    }


    public Epic getEpicById(Long id) {
        return epicRepository.findByEpicId(id);
    }

    
    public List<Epic> getEpicsByProjectId(Long projectId) {
        Project project = projectRepository.findByProjectId(projectId);
        return epicRepository.findAllByProject(project);
    }

    public void removeEpicFromStories(Epic epic) {
        for(Story story : epic.getStories()) {
            storyService.removeEpic(story);
        }
    }

    private boolean userCanAddOrUpdateEpicInProject(User user, Project project) {
        return (user != null && (user.isAdmin()
                || project.getMemberUsernames().contains(user.getUsername())
                || project.getOwnerUsername().equalsIgnoreCase(user.getUsername())));
    }
}
