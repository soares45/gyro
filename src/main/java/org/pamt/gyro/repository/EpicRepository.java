package org.pamt.gyro.repository;


import org.pamt.gyro.bean.Epic;
import org.pamt.gyro.bean.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EpicRepository extends JpaRepository<Epic, Long>{

    //List<Epic> findAll();
    List<Epic> findByTitle(String title);
    Epic findByEpicId(Long EpicId);
    List<Epic> findAllByProject(Project project);
}
