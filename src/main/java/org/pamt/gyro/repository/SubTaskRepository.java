package org.pamt.gyro.repository;

import org.pamt.gyro.bean.Story;
import org.pamt.gyro.bean.SubTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubTaskRepository extends JpaRepository<SubTask, Long> {

    List<SubTask> findAll();
    List<SubTask> findAllByStory(Story story);
    SubTask findBySubTaskId(Long subTaskId);
}
