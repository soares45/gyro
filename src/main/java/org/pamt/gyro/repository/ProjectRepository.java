package org.pamt.gyro.repository;

import org.pamt.gyro.bean.Project;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {
	List<Project> findAll();
	List<Project> findAllByTitle(String title);
	Project findByProjectId(Long projectId);
	List<Project> findAllByOwnerUsername(String ownerUsername);
	List<Project> findAllByOwnerUsernameOrMemberUsernames(String ownerUsername, List<String> memberUsernames);
}
