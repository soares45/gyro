package org.pamt.gyro.repository;

import org.pamt.gyro.bean.Project;
import org.pamt.gyro.bean.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	
	List<User> findAll();
	User findByEmail(String email);
	User findById(Long userId);
	User findByUsername(String username);

	@Query("select u.username from User u")
	List<Object> getUsersUsername();

	void deleteByUsername(String username);
	
	int countByUsername(String username);
	
}
