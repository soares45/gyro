package org.pamt.gyro.repository;

import org.pamt.gyro.bean.Project;
import org.pamt.gyro.bean.Story;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface StoryRepository extends JpaRepository<Story, Long> {
	
	List<Story> findAll();
	List<Story> findAllByProject(Project project);
	Story findByStoryId(Long storyId);
}
