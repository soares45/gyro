module.exports = {
    entry: {
        app: './src/App.js',
        routes: './src/components/Routes.jsx',
        form: './src/components/Form.jsx',
        login: './src/components/Login.jsx',
        project: './src/components/Project.jsx',
        signup: './src/components/SignUp.jsx',
        epic: './src/components/Epic.jsx',
        story: './src/components/Story.jsx',
        subtask: './src/components/Subtask.jsx',
        actiondialog: './src/components/ActionDialog.jsx',
        home: './src/components/Home.jsx',
        menu: './src/components/Menu.jsx'
    },
    output: {
        filename: '[name].bundle.js',
        path: __dirname + '/OUTPUT_TEST'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env', 'react']
                    }
                }
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            }
        ]
    }
};
