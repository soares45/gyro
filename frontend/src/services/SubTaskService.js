import axios from 'axios';
import config from '../utils/config.json';
import $ from 'jquery';

axios.defaults.withCredentials = true;

export default class SubTaskService {

    static createSubTask(storyId, title) {
        return axios.post(`${config.apiUrl}/createSubTask`, $.param({
            storyId: storyId,
            title: title
        }));
    }

    static updateStatus(subTaskId, status) {
        return axios.post(`${config.apiUrl}/updateSubTaskStatus`, $.param({
            subTaskId: subTaskId,
            status: status
        }));
    }

    static updateSubTask(subTaskId, title) {
        return axios.post(`${config.apiUrl}/updateSubTask`, $.param({
            subTaskId: subTaskId,
            title: title
        }));
    }

    static deleteSubTask(subTaskId) {
        return axios.post(`${config.apiUrl}/deleteSubTask`, $.param({
            subTaskId: subTaskId
        }));
    }
}