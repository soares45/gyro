import axios from 'axios';
import config from '../utils/config.json';
import $ from 'jquery';

axios.defaults.withCredentials = true;

export default class StoryService {

    static createStory(title, description, projectId, epicId) {
        return axios.post(`${config.apiUrl}/createStory`, $.param({
            title: title,
            description: description,
            projectId: projectId
        })).then((response) => {
            if(epicId && !response.data.ERROR) {
                return StoryService.associateStoryToEpic(response.data.ID, epicId).then((assignResponse) => {
                    return (assignResponse.data.ERROR ? assignResponse : response);
                });
            }
            return response;
        });
    }


    static associateStoryToEpic(storyId, epicId) {
        return axios.post(`${config.apiUrl}/addStoryToEpic`, $.param({
            storyId: storyId,
            epicId: epicId
        }));
    }


    static assignStory(username, storyId) {
        return axios.post(`${config.apiUrl}/assignUserToStory`, $.param({
            username: username,
            storyId: storyId
        }));
    }


    static unassignStory(storyId) {
        return axios.post(`${config.apiUrl}/unassignStory`, $.param({
            storyId: storyId
        }));
    }


    static updateStory(storyId, title, desc, points) {
        return axios.post(`${config.apiUrl}/updateStory`, $.param({
            storyId: storyId,
            title: title,
            desc: desc,
            points: points
        }));
    }


    static deleteStory(storyId) {
        return axios.post(`${config.apiUrl}/deleteStory`, $.param({
            storyId: storyId
        }));
    }

}
