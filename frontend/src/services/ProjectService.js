import axios from 'axios';
import config from '../utils/config.json';
import $ from 'jquery';
import _ from 'lodash';

axios.defaults.withCredentials = true;

export default class ProjectService {

    static createProject(title, description) {
        return axios.post(`${config.apiUrl}/createProject`, $.param({
            title: title,
            description: description
        }));
    }

    static getOwnedProjects() {
        return axios.get(`${config.apiUrl}/getOwnedProjects`);
    }


    static getProjects() {
        return axios.get(`${config.apiUrl}/getProjects`);
    }


    static getProjectById(id) {
        return axios.get(`${config.apiUrl}/getProject`, {
            params: {
                id: id
            }
        });
    }


    static deleteProjectById(id) {
        return axios.get(`${config.apiUrl}/deleteProject`, {
            params: {
                id: id
            }
        });
    }


    static addUserToProject(username, projectId) {
        return axios.get(`${config.apiUrl}/addUserToProject`, {
            params: {
                username: username,
                projectId: projectId
            }
        });
    }

    static getProjectStories(project) {
        return _.remove(_.clone(project.stories), (story) => {
            return !story.epic;
        });
    }

    static updateProject(projectId, title, desc) {
        return axios.post(`${config.apiUrl}/updateProject`, $.param({
            projectId: projectId,
            title: title,
            desc: desc,
        }));
    }
}
