import axios from 'axios';
import config from '../utils/config.json';
import $ from 'jquery';

axios.defaults.withCredentials = true;

export default class UserService {

    static signIn(username, password) {
        return axios.post(`${config.apiUrl}/login`, $.param({
            username: username,
            password: password
        })).then((response) => {
            if(response.data.ERROR === false) {
                localStorage.setItem("USER", JSON.stringify(response.data.USER));
            }
            return response;
        });
    }


    static signUp(username, email, password) {
        return axios.post(`${config.apiUrl}/register`, $.param({
            username: username,
            email: email,
            password: password
        }));
    }


    static getUser() {
        try {
            return JSON.parse(localStorage.getItem("USER"));
        } catch (e) {
            return null;
        }
    }


    static getUsernames() {
        return axios.post(`${config.apiUrl}/getUsernames`);
    }


    static logout() {
        localStorage.removeItem("USER");
        localStorage.clear();
        return axios.get(`${config.apiUrl}/logout`);
    }


    static isUserLoggedIn() {
        return localStorage.getItem("USER") !== null;
    }
}
