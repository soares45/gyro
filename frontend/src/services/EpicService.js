import axios from 'axios';
import config from '../utils/config.json';
import $ from 'jquery';
import _ from 'lodash';

axios.defaults.withCredentials = true;

export default class EpicService {
    static createEpic(title, description, projectId) {
        return axios.post(`${config.apiUrl}/createEpic`, $.param({
            title: title,
            description: description,
            projectId: projectId
        }));
    }

    static getEpicStories(epic, project) {
        return _.remove(_.clone(project.stories), (story) => {
            return (story.epic ? story.epic.epicId === epic.epicId : false);
        });
    }

    static deleteEpic(epicId) {
        return axios.post(`${config.apiUrl}/deleteEpic`, $.param({
            epicId: epicId
        }));
    }
}