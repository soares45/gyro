import React, {Component} from 'react';
import {MuiThemeProvider} from 'material-ui';
import {BrowserRouter} from 'react-router-dom'
import Routes from './components/Routes.jsx';
import {Switch} from "react-router";

class App extends Component {
    render() {
        return (
            <MuiThemeProvider>
                <BrowserRouter>
                    <div id="global_container">
                        <Switch>
                            <Routes/>
                        </Switch>
                    </div>
                </BrowserRouter>
            </MuiThemeProvider>
        );
    }
}

export default App;
