import {ValidatorForm} from 'react-form-validator-core';

export const customValidationRules = [
    function () {
        ValidatorForm.addValidationRule("matchPassword", (value, e) => {
            let password = document.getElementsByName(e)[0].value;
            return value === password;
        });
    },

    function () {
        ValidatorForm.addValidationRule("maxLength", (value, e) => {
            return value.length <= e;
        });
    },

    function () {
        ValidatorForm.addValidationRule("minLength", (value, e) => {
            return value.length === 0 || value.length >= e;
        });
    },

    function() {
        ValidatorForm.addValidationRule("isAlphanumeric", (value) => {
            return value.match(/^[a-z0-9]+$/i);
        });
    }
];