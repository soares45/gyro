import App from '../App';
import SignUp from '../components/SignUp';
import Login from '../components/Login';
import Project from '../components/Project';
import ProjectView from '../components/ProjectView';
import Story from '../components/Story';
import Epic from '../components/Epic';
import Subtask from '../components/Subtask';
import Home from '../components/Home';
import Menu from '../components/Menu';
import Sidebar from '../components/Sidebar';
import Page from '../components/Page';
import NotFound from "../components/NotFound";

export const componentsRegistry = {
    "App": App,
    "SignUp": SignUp,
    "Login": Login,
    "Project": Project,
    "ProjectView": ProjectView,
    "Story": Story,
    "Epic": Epic,
    "Subtask": Subtask,
    "Home": Home,
    "Menu": Menu,
    "Sidebar": Sidebar,
    "Page": Page,
    "NotFound": NotFound
};