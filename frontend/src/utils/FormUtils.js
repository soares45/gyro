import formValidation from './formValidation.json'
import {customValidationRules} from './customValidationRules';

export class FormUtils {
    static loadCustomRules() {
        customValidationRules.forEach((rule) => {
            rule();
        });
    }

    static getValidators(fieldName) {
        return formValidation[fieldName].rules.map((rule) => {
            return rule.validator;
        });
    }

    static getErrorMessages(fieldName) {
        return formValidation[fieldName].rules.map((rule) => {
            return rule.message;
        });
    }
}