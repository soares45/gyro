import _ from 'lodash';

export default class Status {
    static TODO = 0;
    static IN_PROGRESS = 1;
    static DONE = 2;

    static getKey(value) {
        let key;
        _.forEach(Status, (val, k) => {
            if(value === val) {
                key = k;
            }
        });
        return key;
    }
}