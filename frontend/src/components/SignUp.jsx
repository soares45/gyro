import React from 'react';
import {RaisedButton} from 'material-ui';
import Form from './Form.jsx';
import {TextValidator} from 'react-material-ui-form-validator';
import {FormUtils} from '../utils/FormUtils';
import UserService from '../services/UserService';
import ActionDialog from "./ActionDialog";

export default class SignUp extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            username: "",
            password: "",
            confirmPassword: "",
            dialogMessage: "",
            dialogActions: []
        };
    }

    render() {
        return (
            <div className="sign-form">
                <h2>Register</h2>
                <Form onSubmit={this.onSubmit.bind(this)}>
                    <TextValidator name="email"
                                   hintText="Email"
                                   floatingLabelText="Email"
                                   onChange={this.updateState.bind(this)}
                                   validators={FormUtils.getValidators("email")}
                                   errorMessages={FormUtils.getErrorMessages("email")}
                                   value={this.state.email}/>
                    <TextValidator name="username"
                                   hintText="Username"
                                   floatingLabelText="Username"
                                   onChange={this.updateState.bind(this)}
                                   validators={FormUtils.getValidators("username")}
                                   errorMessages={FormUtils.getErrorMessages("username")}
                                   value={this.state.username}/>
                    <TextValidator type="password"
                                   name="password"
                                   hintText="Password"
                                   floatingLabelText="Password"
                                   onChange={this.updateState.bind(this)}
                                   validators={FormUtils.getValidators("password")}
                                   errorMessages={FormUtils.getErrorMessages("password")}
                                   value={this.state.password}/>
                    <TextValidator type="password"
                                   name="confirmPassword"
                                   hintText="Confirm password"
                                   floatingLabelText="Confirm password"
                                   onChange={this.updateState.bind(this)}
                                   validators={FormUtils.getValidators("confirmPassword")}
                                   errorMessages={FormUtils.getErrorMessages("confirmPassword")}
                                   value={this.state.confirmPassword}/>
                    <div className="buttons">
                        <RaisedButton label="Register" primary={true} type="submit"/>
                        <RaisedButton label="Login" onClick={()=>{this.props.history.push("/signin")}} />
                    </div>
                    <ActionDialog title="SignUp" actions={this.state.dialogActions} ref="dialog">
                        {this.state.dialogMessage}
                    </ActionDialog>
                </Form>
            </div>
        );
    }

    updateState(event, value) {
        let name = event.target.name;
        this.setState({
            [name]: value
        });
    }

    onSubmit() {
        UserService.signUp(this.state.username, this.state.email, this.state.password).then((response) => {
            let actions = [{
                label: "Ok"
            }];
            response.data.ERROR === true ? actions[0].onClick = () => {this.refs.dialog.close()} : actions[0].onClick = () => {this.props.history.push("/")};

            this.setState({
                dialogMessage: response.data.MESSAGE,
                dialogActions: actions
            });

            this.refs.dialog.open();
        });
    }
}