import React from 'react';
import Paper from 'material-ui/Paper';
import Snackbar from 'material-ui/Snackbar';
import ProjectService from '../services/ProjectService';
import ActionDialog from './ActionDialog';
import '../styles/Home.css';
import UserService from "../services/UserService";
import Toggleable from "./Toggleable";
import {RaisedButton, Toggle} from "material-ui";
import EpicService from "../services/EpicService";
import AssignedUser from "./AssignedUser";
import _ from 'lodash';

export default class Home extends React.Component {
    //todo: show tasks
    //todo: icons epic/project/stories/tasks
    //todo: onclick, show page details

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            myAssignOnly: false,
            user: UserService.getUser(),
            projects: [],
            dialogTitle: "",
            dialogBody: "",
            dialogActions: []
        };
    }

    componentWillMount() {
        ProjectService.getProjects().then((response) => {
            if (response.data.ERROR === false) {
                this.setState({
                    projects: response.data.PROJECTS
                });
            }
            else {
                this.showErrorMsg(response);
            }
        })
    }

    showErrorMsg(response) {
        let dialogActions = [{
            label: "Ok",
            onClick: () => {
                this.refs.dialog.close()
            }
        }];
        this.setState({
            dialogActions: dialogActions,
            dialogMessage: response.data.MESSAGE,
        });
        this.refs.dialog.open();
    }

    handleChipTap = () => {
        this.setState({
            open: true,
        });
    };

    handleChipClose = () => {
        this.setState({
            open: false,
        });
    };

    renderSnackbar(msg) {
        return (
            <Snackbar
                open={this.state.open}
                message={msg}
                autoHideDuration={3000}
                onRequestClose={this.handleChipClose}
            />
        );
    }

    handleAssignmentTap(){
        this.setState({
            myAssignOnly: !this.state.myAssignOnly
        });
    };

    renderStories(stories) {
        return (stories ? stories.map((story) => {
            return (!this.state.myAssignOnly || this.isAssignedToMe(story) ?
                <Paper className="paper-story" zDepth={1}>
                    <p><b>Story</b> : {story.title}</p>
                    <AssignedUser edit item={story} onUpdate={this.onAssignedUserUpdate.bind(this)}/>
                    <Toggleable toggled>
                        {/*todo: show tasks*/}
                    </Toggleable>
                </Paper> : null
            );
        }) : null);
    }

    onAssignedUserUpdate(item, response) {
        if(!response.data.ERROR) {
            let projects = _.map(this.state.projects, (project) => {
                let index = _.findIndex(project.stories, {storyId: item.storyId});
                if(index !== -1) {
                    project.stories[index].assigneeUsername = item.assigneeUsername;
                }
                return project;
            });

            this.setState({
                projects: projects
            });
        }
    }

    renderEpics(epics, project) {
        return (epics ? epics.map((epic) => {
            return (
                <Paper className="paper-epic" zDepth={2}>
                    <Toggleable toggled title={`Epic : ${epic.title}`}>
                        {this.renderStories(EpicService.getEpicStories(epic, project))}
                    </Toggleable>
                </Paper>
            );
        }) : null);
    }


    renderProjects() {
        return this.state.projects.map((project) => {
            return (
                <Paper className="paper-project" zDepth={3}>
                    <Toggleable toggled title={`Project : ${project.title}`}>
                        <RaisedButton className="btn-view-page" onClick={()=>{this.props.history.push(`/project/${project.projectId}`)}}>View project page</RaisedButton>
                        {this.renderEpics(project.epics, project)}
                        {this.renderStories(ProjectService.getProjectStories(project))}
                    </Toggleable>
                </Paper>
            );
        });
    }

    render() {
        return (
            <div>
                <ActionDialog title="Home" actions={this.state.dialogActions} ref="dialog">
                    {this.state.dialogMessage}
                </ActionDialog>
                <Toggle className="filter" label="Only show my assignements" onClick={this.handleAssignmentTap.bind(this)}/>
                {this.renderProjects()}
            </div>
        );
    }


    isAssignedToMe(story) {
        return (story.assigneeUsername === UserService.getUser().username);
    }
}
