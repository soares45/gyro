import React from 'react';
import {Route} from "react-router";
import UserService from "../services/UserService";
import RedirectRoute from "./RedirectRoute";

export default class AuthRoute extends React.Component {
    render() {
        return UserService.isUserLoggedIn() ? <Route {...this.props}/> :
            <RedirectRoute to={this.props.redirectLink} from={this.props.path}/>;
    }
}