import React from 'react';
import {TextField, RaisedButton} from 'material-ui';
import Form from './Form.jsx';
import ActionDialog from './ActionDialog';
import UserService from '../services/UserService';

export default class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            dialogMessage: "",
            dialogActions: []
        };
    }

    componentWillMount() {
        UserService.logout();
    }
//
    render() {
        return (
            <div className="sign-form">
                <h2>Login</h2>
                <Form onSubmit={this.onSubmit.bind(this)}>
                    <TextField name="username" hintText="Username" floatingLabelText="Username"
                               onChange={this.updateState.bind(this)} />

                    <TextField type="password" name="password" hintText="Password" floatingLabelText="Password"
                               onChange={this.updateState.bind(this)}  />
                    <div>
                        <RaisedButton label="Login" type="submit" primary={true}/>
                        <RaisedButton label="Register" onClick={()=>{this.props.history.push("/signup")}} default={true}/>
                    </div>
                    <ActionDialog title="Login" actions={this.state.dialogActions} ref="dialog">
                        {this.state.dialogMessage}
                    </ActionDialog>
                </Form>
            </div>
        );
    }

    updateState(event, value) {
        let name = event.target.name;
        this.setState({
            [name]: value
        });
    }

    onSubmit(e) {
        e.preventDefault();
        UserService.signIn(this.state.username, this.state.password).then((response) => {
            if (response.data.ERROR === true) {
                this.setState({
                    dialogMessage: response.data.MESSAGE,
                    dialogActions: [{
                        label: "Ok",
                        onClick: () => {
                            this.refs.dialog.close()
                        }
                    }]
                });
                this.refs.dialog.open();
            } else {
                this.props.history.push("/");
            }
        });
    }
}
