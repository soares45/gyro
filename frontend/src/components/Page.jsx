import React from 'react';
import Menu from "./Menu";
import Sidebar from "./Sidebar";
import UserService from "../services/UserService";

export default class Page extends React.Component {
    render() {
        return (
            <div id="global_container">
                <Menu {...this.props} user={ UserService.getUser()} />
                {this.renderSidebar()}
                {this.props.children}
            </div>
        );
    }

    renderSidebar() {
        return (UserService.isUserLoggedIn()) ?
            <Sidebar {...this.props} user={ UserService.getUser()} /> : null;
    }
}
