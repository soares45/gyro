import React from 'react';
import ActionDialog from "./ActionDialog";

export default class NotFound extends React.Component {

    render() {
        return (
            <ActionDialog open title="Not Found" actions={[{label: "Go to home page", onClick: () => {this.props.history.push("/")}}]}>
                The page you're looking for doesn't exist.
            </ActionDialog>
        );
    }
}