import React from 'react';
import {DataTables} from "material-ui-datatables";
import _ from 'lodash'

export default class DataTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: props.items,
            page: 1
        };
    }

    componentWillUpdate(props) {
        if (!this.state.items && props.items) {
            let items = _.map(props.items, (item) => {
                item._show = true;
                return item;
            });
            this.setState({items: items})
        }
        if(props.pagination) {
            let items = this.getItemsToShow(false);
            if(!_.isEmpty(items) && _.isEmpty(this.getItemsInPage(items))){
                this.onPreviousPageClick();
            }
        }
    }

    render() {
        return (
            <DataTables data={this.getItemsToShow(true)}
                        columns={this.renderColumns()}
                        showFooterToolbar={!!this.props.pagination}
                        onSortOrderChange={this.sort.bind(this)}
                        showHeaderToolbar={this.props.filter}
                        title={this.props.title}
                        filterHintText={this.props.filterText}
                        onFilterValueChange={this.filter.bind(this)}
                        {...this.getPaginationProps()}
                        {...this.props}
            />
        );
    }

    getPaginationProps() {
        if (this.props.pagination) {
            return {
                onNextPageClick: this.onNextPageClick.bind(this),
                onPreviousPageClick: this.onPreviousPageClick.bind(this),
                page: this.state.page,
                count: this.getItemsToShow(false).length,
            };
        }
    }

    onNextPageClick() {
        this.setState({
            page: this.state.page + 1
        })
    }

    onPreviousPageClick() {
        this.setState({
           page: this.state.page - 1
        });
    }

    filter(filter) {
        let columns = this.getColumnsToFilter();
        let items = _.map(this.state.items, (item) => {
            let show = true;
            if (filter) {
                show = false;
                _.map(columns, (column) => {
                    let value = (column.render ? column.render(null, item) : item[column.key]);
                    if (value && value.props.children) {
                        value = value.props.children
                    }
                    if (value && value.toLowerCase().indexOf(filter.toLowerCase()) !== -1) {
                        show = true;
                    }
                });
            }
            item._show = show;
            return item;
        });
        this.setState({
            items: items,
            page: 1
        })
    }

    getColumnsToFilter() {
        return _.filter(this.props.columns, {filter: true});
    }

    getItemsToShow(pagination) {
        let items = (!this.props.filter ? this.state.items :
            _.filter(this.state.items, (item) => {
                return item._show;
            }));
        return (pagination && this.props.pagination ? this.getItemsInPage(items) : items);
    }

    getItemsInPage(items) {
        let start = (this.state.page - 1) * this.props.rowSize;
        let end  = start + this.props.rowSize;
        return _.slice(items, start, end);
    }

    sort(key, order) {
        let items = _.sortBy(this.state.items, [key]);
        if (order === "desc") {
            items = _.reverse(items);
        }
        this.setState({
            items: items
        });
    }

    renderColumns() {
        return _.map(this.props.columns, (column) => {
            column.sortable = !!column.sortable;
            if (this.props.filter) {
                column.filter = !!column.filter
            }
            return column;
        });
    }
}
