import React from 'react';
import '../styles/Toggleable.css';
import FontAwesome from 'react-fontawesome';
import _ from 'lodash';

export default class Toggleable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isToggled: !!props.toggled
        };
    }

    render() {
        return (
            <div>
                <div onClick={this.toggle.bind(this)} className="toggle-title">
                    {this.props.title}
                    {this.renderIcon()}
                </div>
                {this.state.isToggled ? null : this.props.children}
            </div>
        );
    }

    renderIcon() {
        return (this.hasChildren() ?
            <FontAwesome className="icon-right" name={this.state.isToggled ? "chevron-up" : "chevron-down"}/> : null)
    }

    hasChildren() {
        let hasChildren = false;
        _.forEach(this.props.children, (child) => {
            if(child || (child && child.length > 0)) {
                hasChildren = true;
                return false;
            }
        });
        return hasChildren;
    }

    toggle() {
        if(this.hasChildren()) {
            this.setState({
                isToggled: !this.state.isToggled
            });
        }
    }
}