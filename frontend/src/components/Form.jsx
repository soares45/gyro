import React from 'react';
import { ValidatorForm } from 'react-form-validator-core';
import {FormUtils} from '../utils/FormUtils';
import '../styles/Form.css';

export default class Form extends React.Component {
    componentWillMount() {
        FormUtils.loadCustomRules();
    }

    render() {
        return (
            <ValidatorForm onSubmit={this.props.onSubmit}>
                {this.props.children}
            </ValidatorForm>
        );
    }
}