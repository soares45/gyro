import React from 'react';
import {Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink} from 'reactstrap';
import UserService from '../services/UserService';
import FontAwesome from 'react-fontawesome';
import '../styles/Menu.css';

export default class Menu extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
        };
    }


    render() {
        return (
            <Navbar color="faded" light toggleable fixed={'top'}>
                <NavbarToggler onClick={this.toggle.bind(this)} />
                <NavbarBrand onClick={()=>{this.props.history.push("/")}}>Gyro</NavbarBrand>
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        {!this.props.user ? null :
                            <NavItem>
                                    <span className="user-login-status nav-link">
                                        {!this.props.user ? "You are not currently connected" : "You are logged in as " + this.props.user.username}
                                    </span>
                            </NavItem>
                        }
                        <NavItem>
                            {this.props.user ?
                                <NavLink onClick={() => {
                                    this.clickLogout();
                                }}>Logout <FontAwesome name='sign-out'/></NavLink>
                                :
                                <NavLink active={this.isActive("/signin")} onClick={()=>{this.props.history.push("/signin")}}>Login <FontAwesome
                                    name='sign-in'/></NavLink>
                            }
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        );
    }


    clickLogout() {
        UserService.logout();
        this.props.history.push("/signin");
    }


    isActive(link) {
        return window.location.pathname === link;
    }


    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
}