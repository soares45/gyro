import React from 'react';
import {Avatar, Chip} from "material-ui";
import GreenHook from '../styles/green_hook.png';
import UserService from "../services/UserService";
import StoryService from "../services/StoryService";
import ActionDialog from "./ActionDialog";

export default class AssignedUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: this.getUsername(props.item),
            dialogTitle: "",
            dialogActions: [],
            dialogBody: ""
        };
    }

    componentWillUpdate(props) {
        let username = this.getUsername(props.item);
        if(this.state.username !== username) {
            this.setState({
                username: username
            });
        }
    }

    render() {
        //todo: load avatar user
        return (
            <div className="wrapper">
                <Chip onClick={this.onClick.bind(this)} className="chip">
                    {this.renderAvatar()}
                    {this.state.username}
                </Chip>
                <ActionDialog title={this.state.dialogTitle}
                              actions={this.state.dialogActions}
                              ref="dialog">
                    {this.state.dialogBody}
                </ActionDialog>
            </div>
        );
    }

    renderAvatar() {
        return (this.state.username !== "Not assigned" ? <Avatar src={GreenHook}/> : null);
    }

    getUsername(item) {
        return item.assigneeUsername || "Not assigned";
    }

    onClick() {
        return (this.props.edit ? this.updateStoryAssignee(this.props.item) : null);
    }

    updateStoryAssignee(story) {
        let username = UserService.getUser().username;
        let promise = (story.assigneeUsername === username ? this.unassignStory(story) : this.assignStory(username, story));
        if (story.assigneeUsername === username) {
            username = "";
        }
        promise.then((response) => {
            if (!response.data.ERROR) {
                story.assigneeUsername = username;
                this.setState({
                    username: this.getUsername(story)
                })
            }
            if(this.props.onUpdate) {
                this.props.onUpdate(story, response);
            }
        });
    }

    assignStory(username, story) {
        return StoryService.assignStory(username, story.storyId).then((response) => {
            if(response.data.ERROR) {
                this.setState({
                    dialogTitle: `Assign me to story ${story.title}`,
                    dialogBody: response.data.MESSAGE,
                    dialogActions: [{label: "Close", onClick: () => {this.refs.dialog.close()}}]
                });
                this.refs.dialog.open();
            }
            return response;
        });
    }

    unassignStory(story) {
        return StoryService.unassignStory(story.storyId).then((response) => {
            this.setState({
                dialogTitle: `Unassigned story ${story.title}`,
                dialogBody: response.data.MESSAGE,
                dialogActions: [{label: "Close", onClick: () => {this.refs.dialog.close()}}]
            });
            return response;
        });
    }
}