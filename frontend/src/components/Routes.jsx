import React from 'react';
import {Route} from 'react-router';
import {componentsRegistry} from '../utils/componentsRegistry.js';
import routes from '../utils/routes.json'
import AuthRoute from "./AuthRoute";
import RouteNotFound from "./RouteNotFound";

export default class Routes extends React.Component {
    render() {
        return (
            <div>
                {this.getRoutesFromJson(routes)}
            </div>
        );
    }

    getRoutesFromJson(routes) {
        return routes.map((route) => {
            let RouteComponent = Route;
            if(typeof route.component === "string"){
                route.component = componentsRegistry[route.component];
            }
            if (route.componentProps) {
                let Component = route.component;
                route.render = () => {return <Component {...route.componentProps} />;};
                delete route.component;
            }

            if(route.requireAuth) {
                RouteComponent = AuthRoute;
                route.redirectLink = "/signin";
            }
            else if(route.notFound) {
                RouteComponent = RouteNotFound;
            }

            return <RouteComponent {...route}/>
        });
    }
}