import React from 'react';
import {RaisedButton} from "material-ui";
import ActionDialog from "./ActionDialog";
import EpicService from "../services/EpicService";

export default class EpicView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dialogTitle: "",
            dialogActions: [],
            dialogBody: ""
        };
    }

    render() {
        return (
            <div>
                <span className="bold">Description</span>
                <p>{this.props.epic.description}</p>
                <span className="bold">Status</span>
                <p>{this.props.epic.status}</p>
                <span className="bold">Points</span>
                <p>{this.props.epic.points}</p>
                <RaisedButton label="Delete" onClick={this.deleteEpic.bind(this)}/>
                <ActionDialog ref="dialog" title={this.state.dialogTitle} actions={this.state.dialogActions}>
                    {this.state.dialogBody}
                </ActionDialog>
            </div>
        );
    }

    deleteEpic() {
        EpicService.deleteEpic(this.props.epic.epicId).then((response) => {
            if (!response.data.ERROR) {
                this.props.onDelete(this.props.epic);
            }
            else {
                this.setState({
                    dialogTitle: `Delete epic ${this.props.epic.title}`,
                    dialogActions: [{
                        label: "Ok",
                        onClick: () => {
                            this.refs.dialog.close();
                        }
                    }],
                    dialogBody: response.data.MESSAGE
                });
                this.refs.dialog.open();
            }
        });
    }
}