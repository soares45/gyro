import React from 'react';
import Form from './Form.jsx';
import {TextValidator} from 'react-material-ui-form-validator';
import {FormUtils} from '../utils/FormUtils';
import {RaisedButton} from 'material-ui';
import ActionDialog from "./ActionDialog";
import SubTaskService from "../services/SubTaskService";
import _ from 'lodash';

export default class Subtask extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ...this.initFieldsState(props),
            dialogActions: [],
            dialogMessage: ""
        };
    }

    initFieldsState(props) {
        return {
            title: props.update.title || ""
        }
    }

    componentWillUpdate(props) {
        if(props.update !== this.props.update) {
            this.setState({
                ...this.initFieldsState(props)
            });
        }
    }

    render() {
        return (
            <Form onSubmit={this.onSubmit.bind(this)}>
                <h4>{this.renderTitle()}</h4>
                <TextValidator name="title"
                               hintText="Title"
                               floatingLabelText="Title"
                               onChange={this.updateState.bind(this)}
                               validators={FormUtils.getValidators("title")}
                               errorMessages={FormUtils.getErrorMessages("title")}
                               value={this.state.title}/>
                <div className="buttons">
                    <RaisedButton label={this.renderButtonTitle()} primary={true} type="submit"/>
                    {this.renderCancelButton()}
                </div>
                <ActionDialog ref="dialog" title={this.renderTitle()} actions={this.state.dialogActions}>
                    {this.state.dialogMessage}
                </ActionDialog>
            </Form>
        );
    }

    renderTitle() {
        return (this.isUpdate() ? "Update Subtask" : "Add Subtask");
    }

    renderButtonTitle() {
        return (this.isUpdate() ? "Update" : "Add");
    }

    renderCancelButton() {
        return (this.props.cancel ? <RaisedButton label="Cancel" onClick={this.props.onCancel.bind(this)}/> : null);
    }

    isUpdate() {
        return !_.isEmpty(this.props.update);
    }

    updateState(event, value) {
        let name = event.target.name;
        this.setState({
            [name]: value
        });
    }

    onSubmit() {
        this.isUpdate() ? this.updateSubtask() : this.addSubtask();
    }

    addSubtask() {
        SubTaskService.createSubTask(this.props.storyId, this.state.title).then((response) => {
            if (response.data.ERROR) {
                this.onError(response.data.MESSAGE)
            }
            this.props.onAdd(response, this.state);
            if(!response.data.ERROR) {
                this.resetForm();
            }
        });
    }

    updateSubtask() {
        SubTaskService.updateSubTask(this.props.update.subTaskId, this.state.title).then((response) => {
            if (response.data.ERROR) {
                this.onError(response.data.MESSAGE)
            }
            this.props.onUpdate(response, this.state);
        });
    }

    onError(message) {
        this.setState({
            dialogActions: [{
                label: "Ok",
                onClick: () => {this.refs.dialog.close();}
            }],
            dialogMessage: message
        });
    }

    resetForm() {
        this.setState({
            ...this.initFieldsState(this.props)
        });
    }
}