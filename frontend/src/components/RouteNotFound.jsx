import React from 'react';
import routes from '../utils/routes.json';
import _ from 'lodash';
import {Route} from "react-router";
import {matchPath} from "react-router-dom";

export default class RouteNotFound extends React.Component {
    render() {
        return (this.locationExists() ? null :
            <Route {...this.props}/>);
    }

    locationExists() {
        let exists = false;
        _.forEach(routes, (route) => {
            if (this.locationMatches(route)) {
                exists = true;
                return false;
            }
        });
        return exists;
    }

    locationMatches(route) {
        if(route.notFound){
            return false;
        }
        return route.exact && matchPath(window.location.pathname, route);
    }
}