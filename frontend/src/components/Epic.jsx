import React from 'react';
import Form from './Form.jsx';
import {TextValidator} from 'react-material-ui-form-validator';
import {FormUtils} from '../utils/FormUtils';
import {MenuItem, RaisedButton, SelectField} from 'material-ui';
import ProjectService from '../services/ProjectService';
import ActionDialog from "./ActionDialog";
import _ from 'lodash';
import EpicService from "../services/EpicService";

export default class Epic extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            project: "",
            projects: [],
            dialogActions: [],
            dialogMessage: ""
        };
    }

    componentWillMount() {
        ProjectService.getProjects().then((response) => {
            let dialogActions = [{
                label: "Ok",
                onClick: ()=>{this.props.history.push("/")}
            }];
            let dialogMessage = "";
            let project = "";
            let projects = [];
            if (response.data.ERROR === true) {
                dialogMessage = response.data.MESSAGE;
                this.refs.dialog.open();
            } else if (response.data.PROJECTS.length === 0) {
                dialogMessage = "You must have a project to create a story";
                dialogActions.unshift({
                    label: "Create project",
                    onClick: ()=>{this.props.history.push("/create/project")}
                });
                this.refs.dialog.open();
            }
            else {
                projects = _.sortBy(response.data.PROJECTS, ['title']);
                projects = projects.map((project) => {
                    return <MenuItem value={project.projectId} key={project.projectId} primaryText={project.title}/>
                });
                project = projects[0].props.value;
            }

            this.setState({
                project: project,
                projects: projects,
                dialogMessage: dialogMessage,
                dialogActions: dialogActions
            });
        });
    }

    render() {
        return (
            <div className="create-form">
                <Form onSubmit={this.onSubmit.bind(this)}>
                    <h2>Create New Epic</h2>
                    <TextValidator name="title"
                                   hintText="Title"
                                   floatingLabelText="Title"
                                   onChange={this.updateState.bind(this)}
                                   validators={FormUtils.getValidators("title")}
                                   errorMessages={FormUtils.getErrorMessages("title")}
                                   value={this.state.title}/>
                    <TextValidator name="description"
                                   hintText="Description"
                                   floatingLabelText="Description"
                                   multiLine={true}
                                   rows={4}
                                   onChange={this.updateState.bind(this)}
                                   validators={FormUtils.getValidators("description")}
                                   errorMessages={FormUtils.getErrorMessages("description")}
                                   value={this.state.description}/>
                    <SelectField value={this.state.project}
                                 onChange={(event, index, value) => {
                                     this.setState({"project": value})
                                 }}
                                 maxHeight={200}>
                        {this.state.projects}
                    </SelectField>
                    <div className="buttons">
                        <RaisedButton label="Create" primary={true} type="submit" />
                    </div>
                    <ActionDialog title="Story" actions={this.state.dialogActions} ref="dialog">
                        {this.state.dialogMessage}
                    </ActionDialog>
                </Form>
            </div>
        );
    }

    updateState(event, value) {
        let name = event.target.name;
        this.setState({
            [name]: value
        });
    }

    onSubmit() {
        EpicService.createEpic(this.state.title, this.state.description, this.state.project).then((response) => {
            let dialogActions = [{
                label: "Ok",
                onClick: () => {
                    this.refs.dialog.close()
                }
            }];
            this.setState({
                dialogActions: dialogActions,
                dialogMessage: response.data.MESSAGE
            });
            this.refs.dialog.open();
            //todo: redirect if no error
        });
    }
}