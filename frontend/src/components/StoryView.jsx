import React from 'react';
import AssignedUser from "./AssignedUser";
import DataTable from "./DataTable";
import {MenuItem, RaisedButton, SelectField} from "material-ui";
import SubTaskService from "../services/SubTaskService";
import Status from "../utils/Status";
import _ from 'lodash';
import ActionDialog from "./ActionDialog";
import Subtask from "./Subtask";
import FontAwesome from 'react-fontawesome';

export default class StoryView extends React.Component {
    constructor(props) {
        super(props);
        let subtasks = props.story.subtasks;
        this.state = {
            subtasks: subtasks,
            statuses: this.initStatuses(subtasks),
            updateSubtask: {},
            dialogTitle: "",
            dialogActions: [],
            dialogBody: ""
        };
    }

    initStatuses(subtasks) {
        let statuses = {};
        _.forEach(subtasks, (subtask) => {
            statuses[subtask.subTaskId] = Status[subtask.status];
        });
        return statuses;
    }

    render() {
        let columns = [
            {
                key: "subTaskId",
                label: "ID"
            },
            {
                key: "title",
                label: "Title"
            },
            {
                key: "status",
                label: "Status",
                render: (i, item) => {
                    return this.renderSelectStatus(item);
                }
            },
            {
                key: "action",
                label: "Action",
                render: (i, item) => {
                    return (
                        <span>
                            <RaisedButton onClick={() => {
                                this.editSubtask(item)
                            }} label={<FontAwesome name='pencil-square'/>}/>
                             <RaisedButton onClick={() => {
                                 this.deleteSubtask(item)
                             }} label={<FontAwesome name='trash'/>}/>
                        </span>
                    )
                }
            }
        ];
        return (
            <div className="story-dialog">
                <p><b>Description:</b> {this.props.story.description}</p>
                <p><b>Status:</b> {this.props.story.status}</p>
                <p>
                    <b>Assignee</b>
                    <AssignedUser item={this.props.story} edit onUpdate={this.props.onAssignedUserUpdate.bind(this)}/>
                </p>
                <h4>Subtasks</h4>
                <DataTable columns={columns} items={this.state.subtasks} pagination rowSize={3} rowSizeList={[3]}/>
                <Subtask storyId={this.props.story.storyId}
                         onAdd={this.onAddSubtask.bind(this)}
                         onUpdate={this.onUpdateSubtask.bind(this)}
                         update={this.state.updateSubtask}
                         cancel={!_.isEmpty(this.state.updateSubtask)}
                         onCancel={this.onUpdateSubtaskCancel.bind(this)}/>
                <ActionDialog title={this.state.dialogTitle}
                              actions={this.state.dialogActions}
                              ref="dialog">
                    {this.state.dialogBody}
                </ActionDialog>
            </div>
        );
    }

    editSubtask(subtask) {
        this.setState({
            updateSubtask: subtask
        });
    }

    deleteSubtask(subtask) {
        this.onUpdateSubtaskCancel();
        SubTaskService.deleteSubTask(subtask.subTaskId).then((response) => {
           if(!response.data.ERROR) {
               let subtasks = this.state.subtasks;
               _.remove(subtasks, {subTaskId: subtask.subTaskId});
               this.setState({subtasks: subtasks});
           }
           else {
               this.setState({
                   dialogTitle: "Delete subtask",
                   dialogBody: response.data.MESSAGE,
                   dialogActions: [{
                       label: "Ok",
                       onClick: () => {this.refs.dialog.close();}
                   }]
               });
               this.refs.dialog.open();
           }
        });
    }

    onUpdateSubtask(response, fields) {
        if(!response.data.ERROR) {
            let subtasks = this.state.subtasks;
            let index = _.findIndex(subtasks, {subTaskId: this.state.updateSubtask.subTaskId});
            subtasks[index].title = fields.title;
            this.setState({
               subtasks: subtasks
            });
        }
        this.onUpdateSubtaskCancel();
    }

    onUpdateSubtaskCancel() {
        this.setState({
            updateSubtask: {}
        });
    }

    onAddSubtask(response, fields) {
        if (response && !response.data.ERROR) {
            let subtasks = this.state.subtasks;
            subtasks.push({
                subTaskId: response.data.ID,
                title: fields.title,
                status: Status.getKey(Status.TODO)
            });
            this.setState({
                subtasks: subtasks,
                statuses: this.initStatuses(subtasks)
            });
        }
    }

    renderSelectStatus(subtask) {
        return (
            <SelectField value={this.state.statuses[subtask.subTaskId]}
                         autoWidth
                         fullWidth
                         className="status-field"
                         onChange={(event, index, value) => {
                             this.updateStatus(value, subtask.subTaskId)
                         }}
            >
                {_.map(Status, (status, value) => {
                    return <MenuItem value={status} primaryText={value}/>
                })}
            </SelectField>
        );
    }

    updateStatus(value, id) {
        let statuses = this.state.statuses;
        let initialStatus = statuses[id];
        statuses[id] = value;
        this.setState({
            statuses: statuses
        });

        value = Status.getKey(value);

        SubTaskService.updateStatus(id, value).then((response) => {
            if (!response.data.ERROR) {
                let index = _.findIndex(this.state.subtasks, {subTaskId: id});
                let subtasks = this.state.subtasks;
                subtasks[index].status = value;
                this.setState({
                    subtasks: subtasks
                });
            }
            else {
                statuses = this.state.statuses;
                statuses[id] = initialStatus;
                this.setState({
                    statuses: statuses,
                    dialogTitle: "Update subtask status",
                    dialogActions: [{
                        label: "Ok",
                        onClick: () => {
                            this.refs.dialog.close()
                        }
                    }],
                    dialogBody: response.data.MESSAGE
                });
                this.refs.dialog.open();
            }
        });
    }
}