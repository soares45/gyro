import React from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import '../styles/Home.css';
import UserService from "../services/UserService";

const styles = {
    sideBar : {
        zIndex: '50',
        paddingRight: "100px"
    },
    topFiller: {
        height: '56px'
    },
    appNameItem: {
        backgroundColor: '#f7f7f7',
        height: '56px',
        verticalAlign: 'center',
        boxShadow: '0 2px 8px #888888',
        fontSize: '25px',
        paddingTop: '10px',
        paddingLeft: '15px',
        fontFamily: 'Roboto, "Helvetica Neue", Arial, sans-serif'
    },
    appNameItemButton : {
        textDecoration: "none",
        color: 'black',
        cursor: "pointer"
    },
};

export default class Sidebar extends React.Component {
    /* Todo : Cree l'option pour rajouter des MenuItems selon la page */

    /*
    * Home page
    * <MenuItem>*View Current Sprint Only</MenuItem>
    * <MenuItem onClick={this.handleAssignmentTap}>View My Assignment</MenuItem>
    * */
    render() {
        return (UserService.isUserLoggedIn() ?
            <Drawer docked style={styles.sideBar} className={"sidebar"}>
                <div style={styles.topFiller}/>
                <MenuItem onClick={()=>{this.props.history.push("/create/project")}}>Create Project</MenuItem>
                <MenuItem onClick={()=>{this.props.history.push("/create/epic")}}>Create Epic</MenuItem>
                <MenuItem onClick={()=>{this.props.history.push("/create/story")}}>Create Story</MenuItem>
            </Drawer> : null);
    }
}
