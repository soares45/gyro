import React from 'react';
import Form from './Form.jsx';
import {TextValidator} from 'react-material-ui-form-validator';
import {FormUtils} from '../utils/FormUtils';
import {RaisedButton} from 'material-ui';
import ProjectService from '../services/ProjectService';
import ActionDialog from "./ActionDialog";

export default class Project extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            dialogMessage: "",
            dialogActions: []
        };
    }

    render() {
        return (
            <div className="create-form">
                <Form onSubmit={this.onSubmit.bind(this)}>
                    <h2>Create New Project</h2>
                    <TextValidator  name="title"
                                    hintText="Title"
                                    floatingLabelText="Title"
                                    onChange={this.updateState.bind(this)}
                                    validators={FormUtils.getValidators("title")}
                                    errorMessages={FormUtils.getErrorMessages("title")}
                                    value={this.state.title} />
                    <TextValidator  name="description"
                                    hintText="Description"
                                    floatingLabelText="Description"
                                    multiLine={true}
                                    rows={4}
                                    onChange={this.updateState.bind(this)}
                                    validators={FormUtils.getValidators("description")}
                                    errorMessages={FormUtils.getErrorMessages("description")}
                                    value={this.state.description}/>
                    <div className="buttons">
                        <RaisedButton label="Create" primary={true} type="submit" />
                    </div>
                    <ActionDialog title="Project" actions={this.state.dialogActions} ref="dialog">
                        {this.state.dialogMessage}
                    </ActionDialog>
                </Form>
            </div>
        );
    }


    updateState(event, value) {
        let name = event.target.name;
        this.setState({
            [name]: value
        });
    }


    onSubmit() {
        ProjectService.createProject(this.state.title, this.state.description).then((response) => {
            if (response.data.ERROR === true) {
                this.setState({
                    dialogMessage: response.data.MESSAGE,
                    dialogActions: [{
                        label: "Ok",
                        onClick: () => {this.refs.dialog.close()}
                    }]
                });
                this.refs.dialog.open();
            } else {
                // Redirect to created project
                this.props.history.push('/project/'+response.data.ID);
            }
        });
    }
}