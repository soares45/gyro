import React from 'react';
import {Redirect} from "react-router";

export default class RedirectRoute extends React.Component {
    render() {
        return (window.location.pathname === this.props.from ? <Redirect to={this.props.to}/> : null);
    }
}