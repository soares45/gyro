import React from 'react';
import {FlatButton} from "material-ui";
import {Dialog} from 'material-ui';

export default class ActionDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: !!this.props.open
        };
    }

    render() {
        return (
            <div>
                <Dialog
                    title={this.props.title}
                    actions={this.formatActions(this.props.actions)}
                    modal={true}
                    open={this.state.open}>
                    {this.props.children}
                </Dialog>
            </div>
        );
    }

    formatActions(actions) {
        return actions.map((action) => {
            return (
                <FlatButton
                    {...action}
                    primary={true}
                />
            );
        });
    }

    open() {
        this.setState({
            open: true
        });
    }

    close() {
        this.setState({
            open: false
        });
    }
}