import React from 'react';
import ProjectService from '../services/ProjectService';
import UserService from '../services/UserService';
import {RaisedButton} from 'material-ui';
import FontAwesome from 'react-fontawesome';
import {Button} from 'react-bootstrap';
import 'bootstrap-css';
import Autocomplete from 'react-autocomplete';
import DataTable from "./DataTable";
import ActionDialog from "./ActionDialog";
import EpicView from "./EpicView";
import $ from 'jquery';
import StoryService from "../services/StoryService";
import _ from 'lodash';
import StoryView from "./StoryView";
import AssignedUser from "./AssignedUser";


export default class ProjectView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.projectId,
            value: "", // Empty by default, this is used by the Autocomplete component
            editable: "", // Empty by default, this is used by the input elements
            items: [],
            user: UserService.getUser(),
            project: {},
            dialogTitle: "",
            dialogBody: "",
            dialogActions: []
        };
        this.getProject();
        this.getAllUsernames();
    }


    render() {
        return (
            <div className="create-form">
                <h1>{this.state.project.title}</h1>
                <b>Project owner</b>: {this.state.project.ownerUsername}<br/>
                <b>Project members</b>: {this.state.project.memberUsernames}<br/>
                {this.renderAddMemberInputs()}
                <br/>
                <h3>Description</h3>
                {this.renderEditDescriptionInputs()}
                <br/>
                <h3>Stories
                    <Button title="Add Story" onClick={() => {this.props.history.push("/create/story")}}> + </Button>
                </h3>
                {this.renderStories()}
                {(this.state.user.isAdmin || this.state.user.username === this.state.project.ownerUsername) ?
                    <RaisedButton label="Delete" onClick={() => {this.deleteProject()}} /> : ""}
                <ActionDialog title={this.state.dialogTitle} actions={this.state.dialogActions} ref="dialog">
                    {this.state.dialogBody}
                </ActionDialog>
            </div>
        );
    }


    renderStories() {
        let columns = [
            {
                key: "title",
                label: "Title",
                sortable: true
            },
            {
                key: "description",
                label: "Description",
                sortable: true
            },
            {
                key: "points",
                label: "Points",
                sortable: true
            },
            {
                key: "status",
                label: "Status",
                sortable: true
            },
            {
                key: "epic",
                label: "Epic",
                render: (i, item) => {
                    let epic = item.epic;
                    return (epic ? <div className="link" onClick={() => {
                        this.showEpic(epic)
                    }}>{epic.title}</div> : null);
                },
                filter: true,
                sortable: true
            },
            {
                key: "assignee",
                label: "Assignee",
                render: (i, item) => {
                    return <AssignedUser item={item} edit />
                }
            },
            {
                key: "action",
                label: "Action",
                render: (i, item) => {
                    return (
                        <span>
                            <RaisedButton onClick={() => {this.showStory(item)}} label={<FontAwesome name='arrow-circle-right' />}
                                          style={{minWidth:"30px"}} />
                            <RaisedButton onClick={() => {this.editStory(item)}} label={<FontAwesome name='pencil-square'/>}
                                          style={{minWidth:"30px"}} />
                            <RaisedButton onClick={() => {this.deleteStory(item)}} label={<FontAwesome name='trash-o'/>}
                                          style={{minWidth:"30px"}} />
                        </span>
                    )
                }
            }
        ];
        return <DataTable columns={columns} items={this.state.project.stories} filter filterText="Search by epic" pagination rowSize={5} rowSizeList={[5]}/>;
    }


    onAssignedUserUpdate(item, response) {
        if(!response.data.ERROR) {
            let project = this.state.project;
            let index = _.findIndex(project.stories, {storyId: item.storyId});
            project.stories[index].assigneeUsername = item.assigneeUsername;

            this.setState({
                project: project
            });
        }
    }


    renderAddMemberInputs() {
        return (
            <div>
                <Autocomplete
                    getItemValue={(item) => item.label}
                    items={this.state.items}
                    renderItem={(item, isHighlighted) =>
                        <div style={{background: isHighlighted ? 'lightgray' : 'white'}}>
                            {item.label}
                        </div>
                    }
                    value={this.state.value}
                    onChange={e => this.setState({value: e.target.value})}
                    onSelect={value => this.setState({value})}
                />
                <Button title={"Add a member to the project"} onClick={() => {
                    this.addMember();
                }}>
                    <FontAwesome name='user-plus'/>
                </Button>
            </div>

        );
    }


    renderEditDescriptionInputs() {
        return (
            <div>
                <textarea value={this.state.editable} onChange={e => this.setState({editable: e.target.value})} rows="4"
                          style={{width: "80%"}}/>
                <Button title={"Change project description"} onClick={() => {
                    this.updateProject();
                }} style={{marginBottom: "15px", marginLeft: "5px"}}>
                    <FontAwesome name='pencil'/>
                </Button>
            </div>

        );
    }


    addMember() {
        ProjectService.addUserToProject(this.state.value, this.state.id).then((response) => {
            let data = JSON.parse(response.data);
            if (!data.ERROR) {
                this.getProject();
            }
            alert(data.MESSAGE);
            $("#userNameInput").val("");
        });
    }


    getAllUsernames() {
        let items = [];
        UserService.getUsernames().then((response) => {
            let usernames = response.data;
            for (let un in usernames) {
                items.push({label: usernames[un]})
            }
            this.setState({
                items: items
            });
        });
    }


    updateState(event, value) {
        let name = event.target.name;
        this.setState({
            [name]: value
        });
    }


    getProject() {
        ProjectService.getProjectById(this.state.id).then((response) => {
            if (!response.data) {
                alert("Bad project ID"); // TODO: handle error message server-side
            } else {
                this.setState({
                    project: response.data,
                    editable: response.data.description
                });
            }
        });
    }


    deleteProject() {
        ProjectService.deleteProjectById(this.state.id).then((response) => {
            if (response.data) {
                let dialogActions = (response.data.ERROR ? [{
                    label: "Close",
                    onClick: this.closeDialog.bind(this)
                }] : [{
                    label: "Ok", onClick: () => {
                        this.props.history.push("/")
                    }
                }]);
                this.setState({
                    dialogTitle: "Delete",
                    dialogBody: response.data.MESSAGE,
                    dialogActions: dialogActions
                });
                this.refs.dialog.open();
            }
        });
    }


    updateProject() {   // project.title can be editable in the future
        ProjectService.updateProject(this.state.id, this.state.project.title, this.state.editable).then((response) => {
            if (response.data) {
                let dialogActions = (response.data.ERROR ? [{
                    label: "Close",
                    onClick: this.closeDialog.bind(this)
                }] : [{
                    label: "Ok", onClick: () => {
                        ()=>{window.location.reload()}
                    }
                }]);
                this.setState({
                    dialogTitle: "Update",
                    dialogBody: response.data.MESSAGE,
                    dialogActions: dialogActions
                });
                this.refs.dialog.open();
            }
        });
    }


    showStory(story) {
        this.setState({
            dialogActions: [{label: "Close", onClick: this.closeDialog.bind(this)}],
            dialogBody: <StoryView story={story} onAssignedUserUpdate={this.onAssignedUserUpdate.bind(this)}/>,
            dialogTitle: story.title
        });
        this.refs.dialog.open();
    }


    editStory(story) {
        this.setState({
            dialogBody:
                <div className="story-dialog">
                    <label htmlFor="editStoryTitle">Title:
                        <input type="text" id="editStoryTitle" defaultValue={story.title}/>
                    </label>
                    <br/>
                    <label htmlFor="editStoryPoints">Points:
                        <input type="number" id="editStoryPoints" defaultValue={story.points}/>
                    </label>
                    <br/>
                    <label htmlFor="editStoryDescription">Description: <br/>
                        <textarea type="text" id="editStoryDescription" defaultValue={story.description}/>
                    </label>
                    <p><b>Status:</b> {story.status}</p>
                    <RaisedButton label="Save" onClick={() => {this.saveStory(story.storyId)}}/>
                </div>
            ,
            dialogTitle: story.title
        });
        this.refs.dialog.open();
    }


    deleteStory(story) {
        StoryService.deleteStory(story.storyId).then((response) => {
            this.setState({
                dialogTitle: response.data.MESSAGE,
                dialogBody: "",
                dialogActions: [{label: "Close", onClick: ()=>{window.location.reload()}}]
            });
            this.refs.dialog.open();
        })
    }


    saveStory(storyId) {
        var title = $("#editStoryTitle").val();
        var desc = $("#editStoryDescription").val();
        var points = $("#editStoryPoints").val();
        StoryService.updateStory(storyId, title, desc, points).then((response) => {
            window.location.reload();
        });
    }


    showEpic(epic) {
        this.setState({
            dialogBody: <EpicView epic={epic} onDelete={this.onEpicDelete.bind(this)}/>,
            dialogTitle: epic.title,
            dialogActions: [{label: "Close", onClick: this.closeDialog.bind(this)}]
        });
        this.refs.dialog.open();
    }

    onEpicDelete(epic) {
        let project = this.state.project;
        _.remove(project.epics, {epicId: epic.epicId});
        _.forEach(project.stories, (story) => {
            if(story.epic && story.epic.epicId === epic.epicId) {
                story.epic = null;
            }
        });
        this.setState({project: project});
        this.refs.dialog.close();
    }


    closeDialog() {
        this.refs.dialog.close();
    }

}
