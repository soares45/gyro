import React from 'react';
import Form from './Form.jsx';
import {TextValidator} from 'react-material-ui-form-validator';
import {FormUtils} from '../utils/FormUtils';
import {MenuItem, RaisedButton, SelectField} from 'material-ui';
import ProjectService from '../services/ProjectService';
import ActionDialog from "./ActionDialog";
import _ from 'lodash';
import StoryService from "../services/StoryService";

export default class Story extends React.Component {
    projects;
    //todo: add to epic
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            project: "",
            projects: [],
            epic: "",
            epics: [],
            dialogActions: [],
            dialogMessage: ""
        };
        this.projects = [];
    }

    componentWillMount() {
        ProjectService.getProjects().then((response) => {
            let dialogActions = [{
                label: "Ok",
                onClick: () => {
                    this.props.history.push("/")
                }
            }];
            let dialogMessage = "";
            let project = "";
            let projects = [];
            if (response.data.ERROR === true) {
                dialogMessage = response.data.MESSAGE;
                this.refs.dialog.open();
            } else if (response.data.PROJECTS.length === 0) {
                dialogMessage = "You must have a project to create a story";
                dialogActions.unshift({
                    label: "Create project",
                    onClick: () => {
                        this.props.history.push("/create/project")
                    }
                });
                this.refs.dialog.open();
            } else {
                this.projects = response.data.PROJECTS;
                projects = this.renderSelect(response.data.PROJECTS, "title", "projectId", "title");
                project = projects[0].props.value;
                this.updateEpics(project);
            }

            this.setState({
                project: project,
                projects: projects,
                dialogMessage: dialogMessage,
                dialogActions: dialogActions
            });
        });
    }

    renderSelect(items, sortColumn, valueColumn, textColumn, emptyChoice) {
        items = _.sortBy(items, [sortColumn]);
        items = items.map((item) => {
            return <MenuItem value={item[valueColumn]} key={item[valueColumn]} primaryText={item[textColumn]}/>
        });
        if(emptyChoice) {
            items.unshift(<MenuItem value={null} primaryText=""/>);
        }
        return items;
    }

    render() {
        return (
            <div className="create-form">
                <Form onSubmit={this.onSubmit.bind(this)}>
                    <h2>Create New Story</h2>
                    <TextValidator name="title"
                                   hintText="Title"
                                   floatingLabelText="Title"
                                   onChange={this.updateState.bind(this)}
                                   validators={FormUtils.getValidators("storyTitle")}
                                   errorMessages={FormUtils.getErrorMessages("storyTitle")}
                                   value={this.state.title}/>
                    <TextValidator name="description"
                                   hintText="Description"
                                   floatingLabelText="Description"
                                   multiLine={true}
                                   rows={4}
                                   onChange={this.updateState.bind(this)}
                                   validators={FormUtils.getValidators("description")}
                                   errorMessages={FormUtils.getErrorMessages("description")}
                                   value={this.state.description}/>
                    <SelectField value={this.state.project}
                                 floatingLabelText="Project"
                                 onChange={(event, index, value) => {
                                     this.setState({"project": value});
                                     this.updateEpics(value);
                                 }}
                                 maxHeight={200}>
                        {this.state.projects}
                    </SelectField>
                    <SelectField value={this.state.epic}
                                 floatingLabelText="Epic"
                                 onChange={(event, index, value) => {
                                     this.setState({"epic": value})
                                 }}
                                 maxHeight={200}>
                        {this.state.epics}
                    </SelectField>
                    <div className="buttons">
                        <RaisedButton label="Create" primary={true} type="submit" />
                    </div>
                    <ActionDialog title="Story" actions={this.state.dialogActions} ref="dialog">
                        {this.state.dialogMessage}
                    </ActionDialog>
                </Form>
            </div>
        );
    }

    updateState(event, value) {
        let name = event.target.name;
        this.setState({
            [name]: value
        });
    }

    updateEpics(id) {
        let project = _.find(this.projects, {projectId: id});
        let epics = this.renderSelect(project.epics, "title", "epicId", "title", true);
        this.setState({
            epic: epics[0].props.value,
            epics: epics
        });
    }

    onSubmit() {
        StoryService.createStory(this.state.title, this.state.description, this.state.project, this.state.epic).then((response) => {
            let dialogActions = [{
                label: "Ok",
                onClick: () => {
                    this.refs.dialog.close()
                }
            }];
            this.setState({
                dialogActions: dialogActions,
                dialogMessage: response.data.MESSAGE
            });
            this.refs.dialog.open();
            //todo: redirect if no error
        });
    }
}